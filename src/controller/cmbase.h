/*! \mainpage               Descrizione Progetto
  * \section    intro_sec   Introduzione
  * Questo progetto e' stato creato per avere un programma cosidetto "controller" che supervisionera'\n
  * la situazione mentre il programma clipboardmanager e' in esecuzione. A dire il vero questo progetto\n
  * effettivamente compila l'eseguibile di clipboardmanager ma alla fine sfrutta una serie di librerie\n
  * fornite esternamente a questo progetto.
  * \section    mod_design  Spiegazione del design modulare
  * Questo progetto e' stato pensato in modo da essere modulare, ovvero da dividere in modo netto i vari\n
  * moduli che comporranno il prodotto finale.
  *     \subsection mod_graph   Grafico dei moduli e delle dipendenze tra loro
  *         \dot
  *         digraph modgraph {
  *             Con [label="Controller Executable" shape=box fontname = "Courier New"]; // Controller Module    (Executable)
  *             Log [label="Log Module" fontname = "Courier New"];                      // Logging Module       (Library)
  *             Ker [label="Kernel Module" fontname = "Courier New"];                   // Kernel Module        (Library)
  *             Ite [label="Items Module" fontname = "Courier New"];                    // Items Module         (Library)
  *             UI  [label="UI Module" fontname = "Courier New"];                       // UI (Menu) Module     (Library)
  *             Con -> UI;                                                              // Controller Dependancies 1 (to UI Module)
  *             Con -> Log;                                                             // Controller Dependancies 2 (to Log Module)
  *             UI -> Ite;                                                              // UI Dependancies 1 (to Items)
  *             UI -> Ker;                                                              // UI Dependancies 2 (to Kernel)
  *             UI -> Log;                                                              // UI Dependancies 3 (to Log)
  *             Ker -> Ite;                                                             // Kernel Dependancies 1 (to Items)
  *             Ker -> Log;                                                             // Kernel Dependancies 2 (to Log)
  *             Ite -> Log;                                                             // Items Dependancies 1 (to Log)
  *         }
  *         \enddot
  * \section    inst_sec    Installazione
  * Per eseguire l'installazione del programma bisogna assicurarsi di avere tutte le librerie necessarie.
  *     \subsection depend      Installazione: Dipendenze Generali (multipiatta)
  *     Le dipendenze su ogni piattaforma su cui funzionera' ClipboardManager saranno:
  *         \li QtGui4
  *         \li QtCore4
  *         \li QxtGui4
  *         \li QxtCore4
  *         \li ClipboardKernel
  *         \li ClipboardItems
  *         \li ClipboardUI
  *         \li log
  *         \li imageformats/qico4 (solo se abbiamo incluso dei file *.ico nel programma)
  *
  *         \subsubsection depend_win32 Installazione:  Dipendenze Microsoft Windows
  *             Se vogliamo eseguire il programma compilato con il compilatore Mingw-32 (Microsoft Windows)\n
  *             dobbiamo aggiungere le seguenti librerie:
  *                 \li mingwm10
  *                 \li libgcc_s_dw2-1
  *             Se invece vogliamo eseguire la versione del programma compilato con MSVC (MicroSoft Visual C++)\n
  *             per ora non funziona quindi non possiamo eseguire una sega nulla...
  *         \subsubsection  depend_unix Installazione:  Dipendenze Unix
  *             Se vogliamo eseguire l'applicazione su Ubuntu e derivati di esso (non ho mai provato su altre distro\n
  *             di linux) buona fortuna perche' e' da un po che non compilo su queste macchine non ho la minima idea\n
  *             di come funzionino xD.
  */

/*! \brief Gestione/generazione della trayicon e del menu contestuale
  *
  * \author Marco Accorinti
  * \date   13/04/2011
  *
  * Questa classe si occupera della gestione e della creazione degli oggetti descritti sopra.\n\n
  * <b>N.B.:</b> Ogni variabile che comincia con <b><tt>CM</tt></b> e' da intendere come prodotto della classe <tt><b>C</b>lipboard<b>M</b>anager</tt>.\n\n
  * Inoltre viene sfruttata la libreria <b>libQxt</b>, http://dev.libqxt.org/libqxt/wiki/Home per <tt><b>QxtGlobalShortcut</b></tt>,
  * vedi http://libqxt.bitbucket.org/doc/tip/qxtglobalshortcut.html per documentazione.\n\n
  * @todo Creare un header di configurazione per metterci nome dell'applicazione, GUID, versione (forse) eccetera.
  * @todo Implementare files di lingua.
  */

#ifndef CMBASE_H
#define CMBASE_H

// COMMONs //
#include "common.h"

// CORE //
#include <QtCore/QObject>
#include <QtCore/QTimer>
// QXT CORE //
#include <QxtCore/QxtLogger>

// GUI //
#include <QtGui/QApplication>
#include <QtGui/QSystemTrayIcon>
#include <QtGui/QMenu>
#include <QtGui/QAction>
// QXT GUI //
#include <QxtGui/QxtGlobalShortcut>

// MENU //
#include "ClipboardUI/include/clipboardmenu.h"

// LOG //
#include "Log/include/cmlog.h"

#ifdef __unix__
#   include <X11/extensions/XTest.h>
#   include <X11/keysym.h>
#   include <X11/keysymdef.h>
#endif
#ifdef __WIN32
#   include <windows.h>
#   include <winuser.h>
#endif //__WIN32

class CMBase : public QObject
{
    Q_OBJECT

public:
    //! Costruttore generico.
    /*!
      * @param[in]  parent  Genitore dell'oggetto.
      */
    CMBase(QObject * parent = 0);
    //! Distruttore generico.
    virtual ~CMBase();

    //! Mostra la tray icon
    void showTrayIcon();

private slots:
    // ACTIONS //
    //! Mostra il dialogo delle preferenze
    void showPreferences();

    // TRAY ICON //
    //! Tray icon attivata
    /*!
      * @param[in]  _reason  "Ragione" dell'attivazione \sa http://doc.qt.nokia.com/latest/qsystemtrayicon.html#ActivationReason-enum
      */
    void on_CMTrayIcon_activationReason(QSystemTrayIcon::ActivationReason _reason);

    // SHORTCUT //
    //! Shortcut attivato.
    void on_CMGlobalShorcut_activated();
    //! Timer dello shortcut finito.
    void on_CMShortcutTimer_timeout();

private:
    //! Inizializza gli oggetti.
    void initialize();
    //! Crea gli oggetti.
    void createObjects();
    //! Distrugge gli oggetti.
    void destroyObjects();

    //! Crea le azioni dell'applicazione (chiusura e preferenze)
    void createActions();

    //! Crea l'icona nella traybar (e il suo menu contestuale).
    void createTrayIcon();

    //! Crea il menu contestuale contenitore degli item copiati.
    void createMenu();

    //! Crea lo shortcut globale per richiamare il menu contestuale.
    void createGlobalShortcut();

    //! Crea il file di log
    void createLog();

    // ACTIONS //
    QAction             * m_CMCloseAppAction;           //! Azione di chiusura applicazione
    QAction             * m_CMPreferencesAction;        //! Azione che mostra le preferenze dell'applicazione

    // TRAY //
    //! Icona nella SystemTray.
    QSystemTrayIcon     * m_CMTrayIcon;
    //! Menu contestuale della SystemTray Icon.
    QMenu               * m_CMTrayIconMenu;

    // CONTEXT MENU //
    //! Menu contestuale, contenitore degli item copiati.
    ClipboardMenu       * m_CMMenu;

    // SHORTCUT //
    QxtGlobalShortcut   * m_CMGlobalShortcut;           //!< Shortcut/Hotkey per mostrare il menu contestuale.
    QTimer              * m_CMShortcutTimer;            //!< Timer per permettere alla combinazione di tasti di essere premuta correttamente.
    short                 m_CMShortcutTriggeredCount;   //!< Contatore che conta il numero di volte che la combinazione � stata premuta.

    // LOG //
    CMLog               * m_CMLog;                      //!< Classe di logging
};

#endif // CMBASE_H
