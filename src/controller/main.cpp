/*! \brief Entry point dell'applicazione
  *
  * \author Marco Accorinti
  * \date   13/04/2011
  *
  * Permette di entrare nel loop degli eventi della <tt>QApplication</tt>
  */

// GUI //
#include <QtGui/QApplication>

// CMBASE//
#include "cmbase.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setAttribute(Qt::AA_DontShowIconsInMenus, false); // Bugfix for Ubuntu.

    // Crea l'oggetto controllore
    CMBase clipboard_manager_base(&a);
    // Mostra la tray icon
    clipboard_manager_base.showTrayIcon();

    return a.exec();
}
