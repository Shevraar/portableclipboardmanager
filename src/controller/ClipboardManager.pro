############################################
#       ------CLIPBOARD_CONTROLLER------   #
#    Project Created by Acco on 10/04/11   #
#       ------CLIPBOARD_CONTROLLER------   #
############################################

CURRENT_VERSION = 0.1.0.0 #Versione scritta a manella
CURRENT_REVISION = $$system(git rev-parse --short HEAD) #Prende l'ultimo commit effettuato sulla repository

LONG_VERSION = "$${CURRENT_VERSION} ($${CURRENT_REVISION})" #LongVersion contiene la CURRENT_VERSION e la CURRENT_REVISION (ultimo commit)
VERSION = "$${CURRENT_VERSION}" #Versione corta.

# ATTENZIONE QUESTA DEFINE NON FUNZIONA CON MSVC (MicroSoft VisualC++)
DEFINES += \'__LONG_VERSION__=\"$${LONG_VERSION}\"\' # crea una DEFINE che verra' usata nel programma.

QT       += core gui # Moduli Qt necessari per la compilazione dell'applicazione

CONFIG += qxt #Libreria libQxt
QXT += core gui

TARGET = cm # Target nel quale verr� compilato il programma
TEMPLATE = app # Tipo di target della compilazione

# Sorgenti
SOURCES += main.cpp \  # Entry point dell'applicazione
    cmbase.cpp

# Header
HEADERS += \
    cmbase.h \
    common.h

# Libraries includepath
# QtLibs e' il percorso dove si trovano tutte le librerie aggiuntive che usero'
win32 {
    win32:INCLUDEPATH += $$(HOMEDRIVE)/QtLibs/
}
unix {
    unix:INCLUDEPATH += $$(HOME)/Developing/QtLibs/
}

CONFIG(debug, debug|release) {
    #DEBUG
    TARGET = $$join(TARGET,,,_debug)
    win32 {

        win32-msvc2008|win32-msvc2010 { #MSVC
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardUI/msvc/ -lClipboardUId0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/msvc/ -llogd0
        }
        win32-g++ { #MINGW32
            win32:DEFINES += WINVER=0x0500 #Per fare funzionare alcune features (comunque questo define e' da spostare nel Kernel)
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardUI/mingw-32/ -lClipboardUId0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/mingw-32/ -llogd0
        }
    }
    unix {
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/ClipboardUI -lClipboardUId
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/Log -llogd
        unix:LIBS += -lX11 -lXtst -lXext
    }
} else {
    #RELEASE
    win32 {
        win32-msvc2008|win32-msvc2010 { #MSVC
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardUI/msvc/ -lClipboardUI0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/msvc/ -llog0
        }
        win32-g++ { #MINGW32
            win32:DEFINES += WINVER=0x0500 #Per fare funzionare alcune features (comunque questo define e' da spostare nel Kernel)
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardUI/mingw-32/ -lClipboardUI0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/mingw-32/ -llog0
        }
    }
    unix {
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/ClipboardUI -lClipboardUI
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/Log -llog
        unix:LIBS += -lX11 -lXtst -lXext
    }
}


RESOURCES += \
    controller_resources.qrc
