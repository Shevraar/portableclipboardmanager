/*! \file   common.h
  * \brief  File di define comuni al progetto ClipboardManager-Base
  *
  * \author Marco Accorinti
  * \date   16/04/2011
  *
  *         Qua dentro sono presenti le define che verranno usate dentro il progetto ClipboardManager-Base
  */

#ifndef CONFIG_H
#define CONFIG_H

#define     APPLICATION_NAME            "ClipboardManager"                          //!< Nome dell'applicazione NO SPACES
#if defined(_MSC_VER) && (_MSC_VER >= 1300)
    #define     APPLICATION_VERSION         "0.0.6.5"                               //!< Versione dell'applicazione Microsoft VisualC++ Compliant.
#else
    #define     APPLICATION_VERSION         __LONG_VERSION__                        //!< Versione dell'applicazione, vedi file .pro per capire come funziona <b>__LONG_VERSION__</b>
#endif
#define     APPLICATION_GUID            "88707220-682A-11E0-8B5E-A5134824019B"      //!< GUID dell'applicazione

#endif // CONFIG_H
