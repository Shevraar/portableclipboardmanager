#include "cmbase.h"

//+CONSTRUCTOR

CMBase::CMBase(QObject * parent):
    QObject(parent)
{
    createLog();

    initialize();
    createObjects();
}

//-CONSTRUCTOR

//+DESTRUCTOR

CMBase::~CMBase()
{
    destroyObjects();
}
//-DESTRUCTOR

//+INITIALIZE
void CMBase::initialize()
{
    qxtLog->info() << __FILE__ << "---Initializing objects/variables";
    //Azzera tutti i puntatori per non creare danni..
    m_CMCloseAppAction = NULL;
    m_CMPreferencesAction = NULL;
    m_CMTrayIcon = NULL;
    m_CMTrayIconMenu = NULL;
    m_CMMenu = NULL;
    m_CMGlobalShortcut = NULL;
    m_CMShortcutTimer = NULL;

    m_CMShortcutTriggeredCount = 0;
}

//-INITIALIZE

//+CREATE
void CMBase::createObjects()
{
    qxtLog->info() << __FILE__ << "---Creating objects";
    // Crea gli oggetti necessari
    createActions();
    createTrayIcon();
    createMenu();
    createGlobalShortcut();
}

//-CREATE

//+DESTROY
void CMBase::destroyObjects()
{
    qxtLog->info() << __FILE__ << "---Destroying objects";
    if(m_CMCloseAppAction)
        delete m_CMCloseAppAction;
    if(m_CMPreferencesAction)
        delete m_CMPreferencesAction;
    if(m_CMTrayIcon)
        delete m_CMTrayIcon;
    if(m_CMMenu)
        delete m_CMMenu;
    if(m_CMTrayIconMenu)
        delete m_CMTrayIconMenu;
    if(m_CMGlobalShortcut)
        delete m_CMGlobalShortcut;
    if(m_CMShortcutTimer)
        delete m_CMShortcutTimer;
    if(m_CMLog)
        delete m_CMLog;
}

//-DESTROY

//+CREATE
//+ACTIONS
void CMBase::createActions()
{
    // CLOSE
    m_CMCloseAppAction = new QAction(this);
    m_CMCloseAppAction->setText(tr("Close"));
    m_CMCloseAppAction->setIcon(QIcon(":/actions/close16"));
    m_CMCloseAppAction->setToolTip(tr("Closes the application."));

    connect(m_CMCloseAppAction, SIGNAL(triggered()),
            qApp, SLOT(quit()));

    // PREFERENCES
    m_CMPreferencesAction = new QAction(this);
    m_CMPreferencesAction->setText(tr("Preferences"));
    m_CMPreferencesAction->setIcon(QIcon(":/actions/preferences16"));
    m_CMPreferencesAction->setToolTip(tr("Shows the Preferences dialog"));

    connect(m_CMPreferencesAction, SIGNAL(triggered()),
            this, SLOT(showPreferences()));
}

//-ACTIONS

//+SYSTEMTRAY
void CMBase::createTrayIcon()
{
    //if(QFontDatabase::addApplicationFont("http://fonts.kernest.com/embed/thryonmanes.ttf") < 0)
    //{
    //    qxtLog->info() << "Couldn't load webfont #1";
    //}
    // TRAY ICON
    m_CMTrayIcon = new QSystemTrayIcon(this);
    //! @todo Icona personalizzabile?
    m_CMTrayIcon->setIcon(QIcon(":/trayicon/paste16"));
    m_CMTrayIcon->setToolTip(QString("%1 %2")
                             .arg(APPLICATION_NAME)
                             .arg(APPLICATION_VERSION));

    connect(m_CMTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(on_CMTrayIcon_activationReason(QSystemTrayIcon::ActivationReason)));

    // TRAY ICON MENU
    m_CMTrayIconMenu = new QMenu;
    m_CMTrayIconMenu->addAction(m_CMPreferencesAction);
    m_CMTrayIconMenu->addSeparator();
    m_CMTrayIconMenu->addAction(m_CMCloseAppAction);

    m_CMTrayIcon->setContextMenu(m_CMTrayIconMenu);
}

//-SYSTEMTRAY

//+MENU
void CMBase::createMenu()
{
    //! @todo Implementare parte principale del programma, menu con oggetti copiati.
    m_CMMenu = new ClipboardMenu();
}

//-MENU

//+GLOBAL SHORTCUT
void CMBase::createGlobalShortcut()
{
    m_CMGlobalShortcut = new QxtGlobalShortcut(this);
    //! @todo Permettere di configurare la sequenza da premere per visualizzare il menu
    m_CMGlobalShortcut->setShortcut(QKeySequence(Qt::CTRL + Qt::ALT + Qt::Key_V));

    connect(m_CMGlobalShortcut, SIGNAL(activated()),
            this, SLOT(on_CMGlobalShorcut_activated()));
}

//-GLOBAL SHORTCUT

//+LOG
void CMBase::createLog()
{
    m_CMLog = new CMLog("Controller", "Clipboard", this);
}

//-LOG
//-CREATE

//+SLOTS
//+PRIVATE
//+ACTION
void CMBase::showPreferences()
{
    //! @todo Implementare dialogo di configurazione/preferenze
}

//-ACTION

//+TRAY ICON
void CMBase::on_CMTrayIcon_activationReason(QSystemTrayIcon::ActivationReason _reason)
{
    switch(_reason)
    {
    case QSystemTrayIcon::MiddleClick:
    case QSystemTrayIcon::DoubleClick:
        m_CMTrayIcon->hide();           // Nasconde la trayicon.
        m_CMCloseAppAction->trigger();  //!< Come se avessi triggerato l'azione di chiusura applicazione
        break;
    default:
        // Se reason finisce qua c'e' un errore
        break;
    }
}

//-TRAY ICON
//+SHORTCUT
void CMBase::on_CMGlobalShorcut_activated()
{
    // Incrementa il numero di volte che lo shorcut e' stato triggerato.
    m_CMShortcutTriggeredCount++;
    // Se il timer non e' stato creato
    if(!m_CMShortcutTimer)
    {
        m_CMShortcutTimer = new QTimer(this);
        /*! Intervallo di 250 millisecondi (0,25 secondi) per premere ancora la combinazione dei tasti Ctrl+V
          * @todo Permettere di configurare questo parametro
          */
        m_CMShortcutTimer->setInterval(250);
        m_CMShortcutTimer->setSingleShot(true);
        connect(m_CMShortcutTimer, SIGNAL(timeout()),
                this, SLOT(on_CMShortcutTimer_timeout()));
        //! Spara il timer.
        m_CMShortcutTimer->start();
    }
}

void CMBase::on_CMShortcutTimer_timeout()
{
#ifdef __WIN32
        HWND    _hwndActiveWindow;          //!< Ultima finestra selezionata
        DWORD   _dwActiveWindowThreadId;    //!< Thread id della finestra selezionata
        DWORD   _dwCurrentThreadID;         //!< Thread id del nostro processo
#endif // __WIN32
    if(m_CMShortcutTriggeredCount > 1)
    {
        //! @todo Inserire gestione dello shorcut triggerato piu di una volta, ovvero mostra il menu contestuale per l'incollamento dell'item selezionato.
        qxtLog->info() << __FILE__ << "---Global-shortcut combination triggered" << "---Showing ClipboardMenu";
#ifdef __WIN32
        _hwndActiveWindow       = GetForegroundWindow();                            // Ultima finestra selezionata
        _dwActiveWindowThreadId = GetWindowThreadProcessId(_hwndActiveWindow, 0);   // Thread id della finestra selezionata
        _dwCurrentThreadID      = GetCurrentThreadId();                             // Thread id del nostro processo
        if(!AttachThreadInput(_dwActiveWindowThreadId, _dwCurrentThreadID, TRUE)) // attach al thread focussato
            qxtLog->error() << __FILE__ << "---Failed to attach to the thread of the window that's currently having the focus.";
#endif // __WIN32
        m_CMMenu->disableClipboard(); // Disabilita la clipboard...
        bool _bItemSelected = m_CMMenu->show();
        if(_bItemSelected)
        {
            qxtLog->info() << __FILE__ << "---Item selected, pasting it.";
            disconnect(m_CMGlobalShortcut, SIGNAL(activated()),
                        this, SLOT(on_CMGlobalShorcut_activated()));
            delete m_CMGlobalShortcut;
            // PASTE EVENT //
#ifdef __unix__
            int Event, Error;
            int Major, Minor;

            // open the display
            Display * D = XOpenDisplay ( NULL );

            // did we get it?
            if ( ! D ) {
                // nope, so show error and abort
                qxtLog->fatal() << __FILE__ << __LINE__ << ": could not open display \"" << XDisplayName ( NULL ) << "\", aborting.";
                return;
            }
            // does the remote display have the Xtest-extension?
            if ( ! XTestQueryExtension (D, &Event, &Error, &Major, &Minor ) ) {
                // nope, extension not supported
                qxtLog->fatal() << __FILE__ << __LINE__ << ": XTest extension not supported on server \"" << DisplayString(D) << "\"";

                // close the display and go away
                XCloseDisplay ( D );
                return;
            }

            XTestFakeKeyEvent(D, XKeysymToKeycode(D, XK_Control_L), True, 10);
            XFlush ( D );
            XTestFakeKeyEvent(D, XKeysymToKeycode(D, XK_V), True, 10);
            XFlush ( D );
            XTestFakeKeyEvent(D, XKeysymToKeycode(D, XK_V), False, 10);
            XFlush ( D );
            XTestFakeKeyEvent(D, XKeysymToKeycode(D, XK_Control_L), False, 10);
            XFlush ( D );

            XTestDiscard ( D );
            XFlush ( D );

            // we're done with the display
            XCloseDisplay ( D );
#endif
#ifdef  __WIN32
            // Mettere questa parte nel Kernel e creare una versione per LINUX
            // (e se ti prende bene pure per MAC (da vedere come fare))
            //HWND _hwndCurrentlyFocusedControl = GetFocus();
            //LRESULT _lstErr = SendMessage(_hwndCurrentlyFocusedControl, WM_PASTE, 0, 0);
            ///
            INPUT  _ctrlPress;
            INPUT  _vPress;
            INPUT  _vRelease;
            INPUT  _ctrlRelease;

            // ctrl key pressed
            _ctrlPress.type = INPUT_KEYBOARD;
            _ctrlPress.ki.wVk = VK_CONTROL;
            _ctrlPress.ki.wScan = MapVirtualKey(VK_CONTROL, 0);
            _ctrlPress.ki.time = 0;
            _ctrlPress.ki.dwFlags = 0;
            _ctrlPress.ki.dwExtraInfo = 0;

            // v key pressed
            _vPress.type = INPUT_KEYBOARD;
            _vPress.ki.wVk = 0x56; // V
            _vPress.ki.wScan = MapVirtualKey(0x56, 0); // V
            _vPress.ki.time = 0;
            _vPress.ki.dwFlags = 0;
            _vPress.ki.dwExtraInfo = 0;

            // v key released
            _vRelease.type = INPUT_KEYBOARD;
            _vRelease.ki.wVk = 0x56; // V
            _vRelease.ki.wScan = MapVirtualKey(0x56, 0); // V
            _vRelease.ki.dwFlags = KEYEVENTF_KEYUP;
            _vRelease.ki.time = 0;
            _vRelease.ki.dwExtraInfo = 0;

            // ctrl key released
            _ctrlRelease.type = INPUT_KEYBOARD;
            _ctrlRelease.ki.wVk = VK_CONTROL;
            _ctrlRelease.ki.wScan = MapVirtualKey(VK_CONTROL, 0);
            _ctrlRelease.ki.dwFlags = KEYEVENTF_KEYUP;
            _ctrlRelease.ki.time = 0;
            _ctrlRelease.ki.dwExtraInfo = 0;

            QList<INPUT> _keys;
            _keys.append(_ctrlPress);
            _keys.append(_vPress);
            _keys.append(_vRelease);
            _keys.append(_ctrlRelease);

            foreach(INPUT _key, _keys)
            {
                int _ret = SendInput(1,
                                     &_key,
                                     sizeof(INPUT));
                if(_ret == 0)
                {
                    qxtLog->error() << __FILE__ << QString("Couldn't send keyboard event -> Key (0x%1)").arg(_key.ki.wVk, 0, 16);
                }
                int _lstErr = GetLastError();
                if(_lstErr != ERROR_SUCCESS)
                    qxtLog->error() << __FILE__ << QString("Couldn't paste - WIN32 API ERROR - %1").arg(_lstErr);

            }
            ///
            //if(_lstErr != ERROR_SUCCESS)
            //    qxtLog->error() << __FILE__ << QString("Couldn't paste - WIN32 API ERROR - %1").arg(_lstErr);
            //
#endif  //__WIN32
            // END PASTE EVENT //
            //m_CMMenu->removeLastItem(); // Rimuovi l'item appena pastato
            createGlobalShortcut();
        }
#ifdef __WIN32
        if(!AttachThreadInput(_dwActiveWindowThreadId, _dwCurrentThreadID, FALSE)) // Detach dal thread del controllo focussato
            qxtLog->error() << __FILE__ << "---Failed to detach to the thread of the window that's currently having the focus.";
#endif // __WIN32
        m_CMMenu->enableClipboard();   // Riabilita la clipboard...
    }

    // Siccome abbiamo a che fare con un timer singleshot
    // disconnetto il segnale proveniente da m_CMShortcutTimer
    // per evitare problemi.
    disconnect(m_CMShortcutTimer, SIGNAL(timeout()),
               this, SLOT(on_CMShortcutTimer_timeout()));
    delete m_CMShortcutTimer;
    m_CMShortcutTimer = NULL;

    // Azzera il contatore delle volte che lo shorcut e' stato triggerato
    m_CMShortcutTriggeredCount = 0;
}
//-SHORTCUT
//-PRIVATE
//-SLOTS

//+SHOW
void CMBase::showTrayIcon()
{
    m_CMTrayIcon->show();
}

//-SHOW
