#-------------------------------------------------
#
# Project created by QtCreator 2012-05-09T20:42:47
#
#-------------------------------------------------

QT       += core gui

TARGET = ClipboardSettings
TEMPLATE = app


SOURCES += main.cpp\
        clipboardsettings.cpp

HEADERS  += clipboardsettings.h

FORMS    += clipboardsettings.ui
