#ifndef CLIPBOARDSETTINGS_H
#define CLIPBOARDSETTINGS_H

#include <QDialog>

namespace Ui {
class ClipboardSettings;
}

class ClipboardSettings : public QDialog
{
    Q_OBJECT
    
public:
    explicit ClipboardSettings(QWidget *parent = 0);
    ~ClipboardSettings();
    
private:
    Ui::ClipboardSettings *ui;
};

#endif // CLIPBOARDSETTINGS_H
