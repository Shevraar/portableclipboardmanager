#include "clipboardsettings.h"
#include "ui_clipboardsettings.h"

ClipboardSettings::ClipboardSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ClipboardSettings)
{
    ui->setupUi(this);
}

ClipboardSettings::~ClipboardSettings()
{
    delete ui;
}
