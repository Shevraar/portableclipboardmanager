#-------------------------------------------------
#
# Project created by QtCreator 2011-05-15T17:20:58
#
#-------------------------------------------------

VERSION = 0.0.7

QT       += core gui

CONFIG += qxt #Libreria libQxt
QXT += core

TARGET = ClipboardUI
TEMPLATE = lib

DEFINES += CLIPBOARDUI_LIBRARY

SOURCES += \
    menu/clipboardmenu.cpp

HEADERS += ClipboardUI_global.h \
    menu/clipboardmenu.h \


# Libraries includepath
# QtLibs e' il percorso dove si trovano tutte le librerie aggiuntive che usero'
win32 {
    win32:INCLUDEPATH += $$(HOMEDRIVE)/QtLibs/
}
unix {
    unix:INCLUDEPATH += $$(HOME)/Developing/QtLibs/
}


#TARGET LOCALE
CONFIG(debug, debug|release) {
   #DEBUG
   TARGET = $$join(TARGET,,,d)
    win32 {
        win32-msvc2008|win32-msvc2010 { #MSVC
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardItems/msvc/ -lClipboardItemsd0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardKernel/msvc/ -lClipboardKerneld0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/msvc/ -llogd0
        }
        win32-g++ { #MINGW32
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardItems/mingw-32/ -lClipboardItemsd0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardKernel/mingw-32/ -lClipboardKerneld0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/mingw-32/ -llogd0
        }
    }
    unix {
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/ClipboardItems/ -lClipboardItemsd
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/ClipboardKernel/ -lClipboardKerneld
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/Log/ -llogd
    }
} else {
   #RELEASE
    win32 {
        win32-msvc2008|win32-msvc2010 { #MSVC
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardItems/msvc/ -lClipboardItems0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardKernel/msvc/ -lClipboardKernel0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/msvc/ -llog0
        }
        win32-g++ { #MINGW32
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardItems/mingw-32/ -lClipboardItems0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardKernel/mingw-32/ -lClipboardKernel0
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/mingw-32/ -llog0
        }
    }
    unix {
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/ClipboardItems/ -lClipboardItems
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/ClipboardKernel/ -lClipboardKernel
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/Log/ -llog
    }
}

#INSTALLAZIONE TARGET (*.dll, *.lib, *.pdb, *.a, etc)
win32 {
    win32-msvc2008|win32-msvc2010 {
        win32:target.path = $$(HOMEDRIVE)/QtLibs/ClipboardUI/msvc #MSVC
    }
    win32-g++ {
        win32:target.path = $$(HOMEDRIVE)/QtLibs/ClipboardUI/mingw-32 #MINGW32
    }
}
unix {
    unix:target.path =  $$(HOME)/Developing/QtLibs/ClipboardUI/
}

#INSTALLAZIONE HEADERS
header_files.files = $$HEADERS

win32 {
    win32:header_files.path = $$(HOMEDRIVE)/QtLibs/ClipboardUI/include
}
unix {
    unix:header_files.path = $$(HOME)/Developing/QtLibs/ClipboardUI/include
}

#INSTALLS e' chiamato quando a make viene passato l'argomento "install"
INSTALLS += target header_files
