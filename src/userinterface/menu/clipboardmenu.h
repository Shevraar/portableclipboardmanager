/*! \brief Classe da cui derivano i menu con clipboard integrata
  *
  * \author Marco Accorinti
  * \date   15/05/2011
  *
  * Questa classe � un menu' contenente "n" [Type]ClipboardItems (dove [Type] e' un tipo definito da noi), permette il copy e il paste degli items suddetti.
  *
  */

#ifndef CLIPBOARDMENU_H
#define CLIPBOARDMENU_H

// Per esportare la libreria.
#include "ClipboardUI_global.h"

// GUI //
#include <QtGui/QMenu>
#include <QtGui/QApplication>
#include <QtGui/QClipboard>

// CORE //
#include <QtCore/QMimeData>
#include <QtCore/QSet>

// LOG //
#include "Log/include/cmlog.h"

// CLIPBOARD KERNEL //
#if defined(CLIPBOARDUI_LIBRARY)
#   include "ClipboardKernel/include/ClipboardKernel.h"
#   include "ClipboardKernel/include/typerecognition.h"
#   include "ClipboardKernel/include/itemdefinition.h"
#endif

// CLIPBOARD ITEMS //
#if !defined(CLIPBOARDUI_LIBRARY)
#   include "ClipboardItems/include/all.h"
#endif

class CLIPBOARDUISHARED_EXPORT ClipboardMenu : public QMenu
{
    Q_OBJECT
public:
    //! Costruttore generico
    /*!
      * @param[in]  parent  Genitore dell'oggetto.
      */
    ClipboardMenu(QWidget *parent = 0);
    //! Distruttore generico
    virtual ~ClipboardMenu();

public slots:
    //! Mostra il menu
    /*!
      @returns TRUE o FALSE se un item e' stato selezionato o meno.
      */
    bool show();

    //! Disabilita la clipboard temporaneamente, ovvero sconnette il segnale di dati cambiati dal listener nostro
    void disableClipboard() { disconnect(m_pClipboard, SIGNAL(dataChanged()), this, SLOT(on_Clipboard_dataChanged())); }

    //! Abilita la clipboard, ovvero riconnette il segnale di dati cambiati al listener nostro
    void enableClipboard() { connect(m_pClipboard, SIGNAL(dataChanged()), this, SLOT(on_Clipboard_dataChanged())); }

    // ! Rimuove l'ultimo item selezionato dal menu
    void removeLastItem() { removeItem(m_pLastItem); }

private slots:
    //! Slot che gestisce un cambiamento dei dati nella clipboard
    void on_Clipboard_dataChanged();

private:
    //! Inizializza gli oggetti/variabili della classe
    void initialize();
    //! Crea (istanzia) gli oggetti della classe
    void createObjects();
    //! Distrugge gli oggetti della classe
    void destroyObjects();

    //! Crea la clipboard (prende la reference da QApplication)
    void createClipboard();
    //! Crea il log della classe
    void createLog();
    //! Crea il SET di CRCs degli item nella clipboard
    void createItemsCRC();
    //! Crea l'istanza del kernel per permettere al kernel di loggare
    void createKernelIstance();

    //! Rimuove un item dal clipboard menu
    void removeItem (ClipboardItem * _item);

    // CLIPBOARD //
    QClipboard      * m_pClipboard;     //!< Istanza statica della clipboard

    // LOG //
    CMLog           * m_pLog;           //!< Oggetto per loggare, istanziato qua ma utilizzato nell'intero progetto ClipboardManager-Base @todo Spostare la creazione del log in un posto piu' in vista.

    // ITEMS CRCs //
    QSet<quint16>   * m_pItemsCrc;      //!< CRC degli items copiati nella clipboard (CRC-16-CCITT)

    // ITEMS //
    ClipboardItem   * m_pLastItem;      //!< Ultimo item selezionato, verra' cancellato se il paste e' avvenuto con successo.
};

#endif // CLIPBOARDMENU_H
