#include "clipboardmenu.h"



//+CONSTRUCTOR
ClipboardMenu::ClipboardMenu(QWidget *parent) :
    QMenu(parent)
{
    createLog();

    initialize();
    createObjects();
}
//-CONSTRUCTOR

//+DESTRUCTOR
ClipboardMenu::~ClipboardMenu()
{
    destroyObjects();
}

//-DESTRUCTOR

//+INITIALIZE
void ClipboardMenu::initialize()
{
    qxtLog->info() << __FILE__ << "---Initializing objects/variables";
    m_pClipboard = NULL;
    m_pItemsCrc = NULL;
}

//-INITIALIZE

//+CREATE
void ClipboardMenu::createObjects()
{
    qxtLog->info() << __FILE__ << "---Creating objects";
    createClipboard();
    createItemsCRC();
    createKernelIstance();
}
//-CREATE

//+DESTROY
void ClipboardMenu::destroyObjects()
{
    qxtLog->info() << __FILE__ << "---Destroying objects";
    // Fa puntare ad una reference NULL, la clipboard verr� distrutta dalla QApplication
    m_pClipboard = NULL;
    if(m_pLog)
        delete m_pLog;
    if(m_pItemsCrc)
        delete m_pItemsCrc;

    // Distrugge l'istanza di ClipboardKernel (smette di loggare)
    ClipboardKernel::Destroy();
}

//-DESTROY

//+CLIPBOARD
void ClipboardMenu::createClipboard()
{
    // QApplication ha una singola istanza della clipboard, facciamo puntare la nostra variabile ad essa.
    m_pClipboard = QApplication::clipboard();
    connect(m_pClipboard, SIGNAL(dataChanged()),
            this, SLOT(on_Clipboard_dataChanged()));
}

//-CLIPBOARD
//+LOG
void ClipboardMenu::createLog()
{
    m_pLog = new CMLog("UserInterface", "Clipboard", this);
}

//-LOG

//+ITEMS CRC
void ClipboardMenu::createItemsCRC()
{
    m_pItemsCrc = new QSet<quint16>;
}

//-ITEMS CRC

//+KERNEL
void ClipboardMenu::createKernelIstance()
{
    // Crea l'istanza di Clipboard Kernel, cio' permette di loggare al modulo clipboard kernel
    ClipboardKernel::Init();
}

//-KERNEL

//-CREATE

//+REMOVE
void ClipboardMenu::removeItem(ClipboardItem *_item)
{
    m_pItemsCrc->remove(_item->identifier());
    removeAction(_item);
    delete _item;
    _item = NULL;
}

//-REMOVE

//+SLOTS
//+PUBLIC
bool ClipboardMenu::show()
{
    if(actions().count() > 0)
    {
        // Mostra il menu e aspetta che l'utente selezioni un elemento
        ClipboardItem * _selectedItem = dynamic_cast<ClipboardItem *>(exec(QCursor::pos() + QPoint(5, 5)));
        if(_selectedItem)
        {
            m_pClipboard->setMimeData(_selectedItem->data());
            m_pLastItem = _selectedItem;
            return true;
        }
    }
    return false;
}
//-PUBLIC
//+PRIVATE
//+CLIPBOARD
void ClipboardMenu::on_Clipboard_dataChanged()
{
    const QMimeData * _pClipboardData = m_pClipboard->mimeData(QClipboard::Clipboard);
    qxtLog->info() << __FILE__ << "---Clipboard data has changed";

    ClipboardItem * _item = ItemDefinition::getItemType(_pClipboardData, m_pItemsCrc);
    if(_item) // se _item esiste
        addAction(_item); // lo aggiunge al menu
}

//-CLIPBOARD
//-PRIVATE
//-SLOTS
