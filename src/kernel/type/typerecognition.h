/*! \file typerecognition.h Collezione di namespace atta al riconoscimento del tipo copiato sulla clipboard
  *
  * \author Marco Accorinti
  * \date   01/05/2011
  *
  * TypeRecognition riconosce il giusto tipo di item per ClipboardMenu
  * Type fornisce un albero dei tipi riconosciuti, cos� come una via di fuga per i tipi non riconosciuti.
  *
  */

#ifndef TYPERECOGNITION_H
#define TYPERECOGNITION_H

// CORE //
#include <QtCore/QObject>
#include <QtCore/QMimeData>
#include <QtCore/QDir>
#include <QtCore/QUrl>
#include <QtCore/QFileInfo>

// COMMON DEFINES and INCLUDES //
#include "common.h"

// DATABASE //
#if defined(CLIPBOARDKERNEL_LIBRARY)
#   include "database/database.h"
#endif

//! Spazio dei nomi dedicato a enum per tipizzare gli items copiati sulla Clipboard
namespace Type
{
    //! Tipo generico
    enum eGeneric
    {
        Text        =   0x00,       //!< Testo
        File        =   0x01,       //!< File/Directory
        Unknown     =   0xff        //!< Tipo sconosciuto
    };

    //! Testo
    enum eText
    {
        PlainText   =   0x00,       //!< Testo Normale
        Html        =   0x01,       //!< Testo Html
        UnknownText =   0xff        //!< Testo Sconosciuto
    };

    //! File/directory
    enum eFile
    {
        Image       =   0x01,       //!< File Immagine
        Video       =   0x02,       //!< File Video
        Audio       =   0x03,       //!< File Audio
        Directory   =   0xfe,       //!< Directory
        UnknownFile =   0xff        //!< File Sconosciuto
    };
};

//! Spazio dei nomi dedicato al riconoscimento del tipo di item adatto
namespace TypeRecognition
{
    //! Selezione multipla?
    /*!
      * @param[in]  _data   Data della clipboard
      * @return             TRUE o FALSE in base al riscontro avuto
      */
    CLIPBOARDKERNELSHARED_EXPORT  bool  hasMultipleSelection(const QMimeData * _data);

    //! Prendi il tipo generico a partire da _data
    /*!
      * @param[in]  _data   Data della clipboard
      * @return Il tipo generico
      * @sa     Documentazione Type::eGeneric
      */
    Type::eGeneric      getGenericMimeType(const QMimeData *_data);
    //! Prendi il tipo di testo a partire da _data
    /*!
      * @param[in]  _data   Data della clipboard
      * @return Il tipo di testo
      * @sa     Documentazione Type::eText
      */
    Type::eText         getTextMimeType(const QMimeData *_data);
    //! Prendi il tipo di file (o directory) a partire da _data
    /*!
      * @param[in]  _data   Data della clipboard
      * @return Il tipo di file (o directory)
      * @sa     Documentazione Type::eFile
      */
    Type::eFile         getFileMimeType(const QMimeData *_data);

    //! Prendi i tipi di files (o directories) a partire da _data
    /*!
      * @param[in]  _data   Data della clipboard
      * @return I tipi di file (o directory)
      * @sa     Documentazione Type::eFile
      */
    QList<Type::eFile>   getFilesMimeType(const QMimeData *_data);

    //! E' un mimetype riconducibile a un file?
    /*!
      * @param[in]  _formats    Formati, passati da MimeData...
      * @returns                TRUE se e' un formato riconosciuto, FALSE se non lo e'
      */
    bool                hasFileMimeType(QStringList _formats);
};

#endif // TYPERECOGNITION_H
