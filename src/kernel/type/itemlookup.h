/*! \brief Namespace dedicato al controllo sugli item della clipboard
  *
  * \author Marco Accorinti
  * \date   03/05/2011
  *
  * ItemLookup controlla se nel set degli item copiati nella clipboard e' gia presente un dato item, o piu' items.
  */

#ifndef ITEMLOOKUP_H
#define ITEMLOOKUP_H

// CORE //
#include <QtCore/QtGlobal>
#include <QtCore/QVector>
#include <QtCore/QSet>

// TYPERECOGNITION //
#include "typerecognition.h"

// ITEM UTILS //
#include "itemutils.h"

// COMMON DEFINES and INCLUDES //
#include "common.h"

namespace ItemLookup
{
    //! L'item e' gia stato copiato?
    /*!
      * @param[in]  _data       Dati dell'item copiato
      * @param[in]  _itemsCrc   Set di CRC16 su cui effettuare la verifica se l'item e' stato gia effettivamente copiato
      * @returns                TRUE o FALSE se item e' gia stato copiato o meno.
      */
    bool                isItemAlreadyCopied         (const QMimeData * _data, QSet<quint16> * _itemsCrc);

    //*! La collection e' stata copiata?
    /*!
      * @param[in]  _data       Dati della collection copiata
      * @param[in]  _itemsCrc   Set di CRC16 su cui effettuare la verifica se la collection e' tata gia effettivamente copiata
      * @returns                TRUE o FALSE se la collection e' gia stata copiata o meno.
      */
    bool                isCollectionAlreadyCopied   (const QMimeData * _data, QSet<quint16> * _itemsCrc);

    //! Gli item sono gia stati copiati?
    /*!
      * @deprecated             Non piu' usato, ora vengono usate le collection per selezioni multiple
      * @param[in]  _data       Dati degli items copiati
      * @param[in]  _itemsCrc   Set di CRC16 su cui effettuare la verifica se gli item e' stato gia effettivamente copiato
      * @returns                Un vettore contenente true o false se un item e' stato copiato o meno
      */
    QVector<bool>      areItemsAlreadyCopied    (const QMimeData * _data, QSet<quint16> * _itemsCrc);
};

#endif // ITEMLOOKUP_H
