#include "typerecognition.h"

bool TypeRecognition::hasMultipleSelection(const QMimeData *_data)
{
    if(_data->hasUrls())
    {
        quint16 _iItemCount = _data->urls().count();
        if(_iItemCount > 1)
            return true;
        else
            return false;
    }
    else
    {
        return false;
    }
}

Type::eGeneric TypeRecognition::getGenericMimeType(const QMimeData *_data)
{
    //Punta ad un percorso?
    if(_data->hasUrls() /*|| hasFileMimeType((_data->formats()))*/)
    {
        // Se stiamo copiando un link, hasUrls ritorna TRUE, pero' non e' un file a tutti gli effetti.
        // Se data ha del testo (text/plain) allora ritorna Text come generic type
        if(_data->hasText())
        {
            return Type::Text;
        }
        else
        {
            return Type::File;
        }
    }
    //Contiene del testo o html?
    else if(_data->hasText() || _data->hasHtml())
    {
        return Type::Text;
    }
    //Tipo sconosciuto
    else
    {
        return Type::Unknown;
    }
}

Type::eText TypeRecognition::getTextMimeType(const QMimeData *_data)
{
    if(_data->hasHtml())
    {
        return Type::Html;
    }
    else if(_data->hasText())
    {
        return Type::PlainText;
    }
    else
    {
        return Type::UnknownText;
    }
}

Type::eFile TypeRecognition::getFileMimeType(const QMimeData *_data)
{
    QFileInfo _fileInfo;
    foreach(QUrl _url, _data->urls())
    {
        _fileInfo.setFile(_url.toLocalFile());

        //Se l'url punta a una directory
        if(_fileInfo.isDir())
        {
            return Type::Directory;
        }
        //Altrimenti punta ad un file
        else
        {
            // Se il file non ha estensione, significa che e' un file del genere README, AUTHORS, MAKEFILE e cosi' via.
            if(_fileInfo.completeSuffix().isEmpty())
            {
                return Type::UnknownFile;
            }
            else
            {
                // Interrogo il database per sapere che tipo di file e'
                QString _query =    "SELECT file.type_id "
                                    "FROM file "
                                    "WHERE file.ext LIKE '%" + _fileInfo.completeSuffix() + "%'";

                QSqlQuery _queryResult = Database::Istance->query(_query, Database::File);
                int _typeId = _queryResult.record().indexOf("type_id");
                if(_queryResult.next()) // Un risultato solo aspettato
                {
                    Type::eFile _type = static_cast<Type::eFile>(_queryResult.value(_typeId).toInt());
                    return _type;
                }
                else
                {
                    //Se non e' riuscito a riconoscere il tipo con i controlli presenti sopra, allora ritorna UnknownFile
                    return Type::UnknownFile;
                }
            }
        }
    }
    //Se non e' riuscito a riconoscere il tipo con i controlli presenti sopra, allora ritorna UnknownFile
    return Type::UnknownFile;
}

QList<Type::eFile> TypeRecognition::getFilesMimeType(const QMimeData *_data)
{
    QList<Type::eFile> _lstItemsType;

    QFileInfo _fileInfo;

    foreach(QUrl _url, _data->urls())
    {
        _fileInfo.setFile(_url.toLocalFile());

        //Se l'url punta a una directory
        if(_fileInfo.isDir())
        {
            _lstItemsType.append(Type::Directory);
        }
        //Altrimenti punta ad un file
        else
        {
            // Se il file non ha estensione, significa che e' un file del genere README, AUTHORS, MAKEFILE e cosi' via.
            if(_fileInfo.completeSuffix().isEmpty())
            {
                _lstItemsType.append(Type::UnknownFile);
            }
            else
            {
                // Interrogo il database per sapere che tipo di file e'
                QString _query =    "SELECT file.type_id "
                                    "FROM file "
                                    "WHERE file.ext LIKE '%" + _fileInfo.completeSuffix() + "%'";

                QSqlQuery _queryResult = Database::Istance->query(_query, Database::File);
                int _typeId = _queryResult.record().indexOf("type_id");
                if(_queryResult.next()) // Un risultato solo aspettato
                {
                    Type::eFile _type = (Type::eFile)_queryResult.value(_typeId).toInt();
                    _lstItemsType.append(_type);
                }
                else
                {
                    //Se non e' riuscito a riconoscere il tipo con i controlli presenti sopra, allora ritorna UnknownFile
                    _lstItemsType.append(Type::UnknownFile);
                }
            }
        }
    }

    return _lstItemsType;
}

bool TypeRecognition::hasFileMimeType(QStringList _formats)
{
    bool _isKnownFileFormat = false;
    // Per matchare i tipi che noi riconosciamo come files...
    /* Il funzionamento e':
      * Hai trovato almeno un formato che combacia con la regular expression?
      * Se si allora assegna true alla variabile booleana _isKnownFileFormat
      * Se non hai trovato nulla allora assegna false alla variabile booleana _isKnownFileFormat
      */
    _isKnownFileFormat = (_formats.filter(QRegExp("(image|video|audio|application)/.*")).count() > 0 ? true : false );
    return _isKnownFileFormat;
}
