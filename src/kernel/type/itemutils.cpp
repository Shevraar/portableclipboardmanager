#include "itemutils.h"

quint16 ItemUtils::calculateCRC16(const QMimeData *_data)
{
    quint16 _crc = 0x0000;
    switch(TypeRecognition::getGenericMimeType(_data))
    {
    case Type::Text:
        _crc = qChecksum(_data->text().toStdString().c_str(), _data->text().toStdString().size());
        break;
    case Type::File:
    {
        if(_data->urls().count() > 1)
        {
            qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) << "---MimeData has more than one url, that means it has a multiple selection, I can't handle that here.";
        }
        else
        {
            QFileInfo _fileInfo(_data->urls().first().toLocalFile());
            _crc = calculateFileCRC16(_fileInfo);
        }
    }
        break;
    case Type::Unknown:
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Couldn't recognize MimeType, wrong assertion brought program here (Unknown)";
        break;
    default:
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Unhandled case in switch (while getting generic type)";
        break;
    }
    return _crc;
}

quint16 ItemUtils::calculateCollectionCRC16(const QMimeData *_data)
{
    quint16 _crc = 0x0000;
    QFileInfo _currentFileInfo;
    std::string _multiSelectionData = ""; // Qua metteremo dati per fare checksum successivamente
    foreach(QUrl _url, _data->urls())
    {
        _currentFileInfo = _url.toLocalFile();
        for(int i = 0; i < 2; i++) // Prendiamo i primi due caratteri dal nome del file
            _multiSelectionData += _currentFileInfo.fileName()[i].toAscii();
    }
    _multiSelectionData += _currentFileInfo.dir().path().toStdString();

    _crc = qChecksum(_multiSelectionData.c_str(), _multiSelectionData.size());

    return _crc;
}

quint16 ItemUtils::calculateFileCRC16(QFileInfo _fileInfo)
{
    quint16 _crc = 0x0000;
    _crc = qChecksum(_fileInfo.absoluteFilePath().toStdString().c_str(), _fileInfo.absoluteFilePath().toStdString().size());
    return _crc;
}
