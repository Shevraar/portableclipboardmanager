/*! \brief  Namespace dedicato alle utilita' sfruttate nel progetto Kernel
  *
  * \author Marco Accorinti
  * \date   15/05/2011
  *
  *  Qua sono definite le utilita' che verranno usate nel progetto ClipboardKernel\n
  *  come il calcolo del crc di un item/file/directory/quello che ca' e'...
  */

#ifndef ITEMUTILS_H
#define ITEMUTILS_H

// CORE //
#include <QtCore/QtGlobal>
#include <QtCore/QMimeData>
#include <QtCore/QFileInfo>

// TYPERECOGNITION //
#include "typerecognition.h"

namespace ItemUtils
{
    //! Calcola il CRC16 dell'item contenuto in data
    /*!
      * @param[in]  _data       Dati degli items copiati
      * @returns                CRC16 basato su _data (e sul tipo di essa)
      */
    quint16             calculateCRC16          (const QMimeData * _data);

    //! Calcola il CRC16 del file
    /*!
      * @param[in]  _fileInfo   Info del file copiato
      * @returns                CRC16 basato su _fileInfo
      */
    quint16             calculateFileCRC16      (QFileInfo _fileInfo);

    //! Calcola il CRC16 della collection
    /*!
      * @param[in]  _data       Dati degli items copiati
      * @returns                CRC16 basato su _data (e sul tipo di essa)
      */
    quint16             calculateCollectionCRC16(const QMimeData * _data);
}

#endif // ITEMUTILS_H
