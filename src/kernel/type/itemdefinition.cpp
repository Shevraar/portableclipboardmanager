#include "itemdefinition.h"

#ifdef _WIN32
#   include <QtGui/QWindowsMime>
#endif

ClipboardItem * ItemDefinition::getItemType(const QMimeData *_data, QSet<quint16> *_itemsCrc)
{
    ClipboardItem * _item = NULL;

    ////////////////////////////
    ////// MULTIPLE ITEMS //////
    ////////////////////////////
    if(TypeRecognition::hasMultipleSelection(_data))
    {
        // Se la collection non e' gia stato copiata...
        // Procedi con la creazione di un'istanza per l'item.
        // Se l'item invece e' gia stato copiato allora procedi fino al return, quindi
        // ritorna un'istanza nulla dell'item.)
        if(!ItemLookup::isCollectionAlreadyCopied(_data, _itemsCrc))
            _item = instanceForMultipleSelection(_data);
    }
    /////////////////////////
    ////// SINGLE ITEM //////
    /////////////////////////
    // Se l'item non e' gia stato copiato...
    // Procedi con la creazione di un'istanza per l'item.
    // Se l'item invece e' gia stato copiato allora procedi fino al return, quindi
    // ritorna un'istanza nulla dell'item.
    else if(!ItemLookup::isItemAlreadyCopied(_data, _itemsCrc))
    {
        switch(TypeRecognition::getGenericMimeType(_data))
        {
        case Type::Text:
        {
            _item = instanceForTextType(TypeRecognition::getTextMimeType(_data), _data);
            break;
        }
        case Type::File:
        {
            QFileInfo _fileInfo(_data->urls().first().toLocalFile());
            _item = instanceForFileType(TypeRecognition::getFileMimeType(_data), _fileInfo, _data);
            break;
        }
        case Type::Unknown:
            qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Couldn't recognize MimeType (Unknown)";
            return NULL;
        default:
            qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Unhandled case in switch (while getting generic type)";
            return NULL;
        }

        // Aggiorna il set dei vari CRC16 aggiungendo il crc dell'ultimo item creato
        *_itemsCrc << _item->identifier();
    }

    // Ritorna l'item specializzato in base al tipo riconosciuto
    return _item;
}

QList<ClipboardItem *> ItemDefinition::getItemsType(const QMimeData *_data, QSet<quint16> * _itemsCrc)
{
    QList<ClipboardItem *> _items;

    // Controllo a monte per evitare errori
    switch(TypeRecognition::getGenericMimeType(_data))
    {
    case Type::Text:
        // Non dovrebbe mai andare qua dentro, se ci va c'e' un errore
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Wrong assertion, multiple items shouldn't be text items";
        break;
    case Type::File:
    {
        QList<Type::eFile> _itemsType = TypeRecognition::getFilesMimeType(_data); // Lista dei tipi di items
        QVector<bool>  _copiedItems = ItemLookup::areItemsAlreadyCopied(_data, _itemsCrc); // Vettore con gli items gia copiati
        QList<QUrl> _itemUrls = _data->urls(); // Lista di url degli item che vogliamo copiare
        quint16 _iCurrentItem = 0; // Azzera l'iteratore
        foreach(Type::eFile _itemType, _itemsType)
        {
            // Se il vettore alla posizione attuale (_iCurrentItem) dice che l'item e'
            // gia stato copiato allora salta un giro e vai al prossimo item.
            if(_copiedItems.at(_iCurrentItem) == true)
            {
                qxtLog->info() << __FILE__ << QString("--Item %1 is already copied").arg(_itemUrls.at(_iCurrentItem).toLocalFile());
            }
            // Altrimenti significa che l'item non e' stato copiato quindi crea una nuova istanza dell'item
            // e appendilo alla lista di items
            else
            {
                QFileInfo _fileInfo(_itemUrls.at(_iCurrentItem).toLocalFile());
                _items.append(instanceForFileType(_itemType, _fileInfo, _data));
                *_itemsCrc << _items.last()->identifier();
                qxtLog->info() << __FILE__ << QString("--Putting item into the list.\t%1").arg(_itemType );
            }
            // Vai al prossimo item..
            _iCurrentItem++;
        }
    }
        break;
    case Type::Unknown:
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Couldn't recognize MimeType, wrong assertion brought program here (Unknown)";
        break;
    default:
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Unhandled case in switch (while getting generic type)";
        break;
    }

    // Ritorna gli items specializzati in base ai tipi riconosciuti (se la lista e' stata popolata)
    return _items;
}

ClipboardItem * ItemDefinition::instanceForFileType(Type::eFile _type, QFileInfo _fileInfo, const QMimeData *_data)
{
    ClipboardItem * _item = NULL;

    switch(_type)
    {
    case Type::Image:
        qxtLog->info() << __FILE__ << "--Instancing new ImageClipboardItem.\t(Image)";
        _item = new ImageClipboardItem();
        _item->setName(_fileInfo.completeBaseName());
        //! @todo Crea un oggetto QImage che verra' poi visualizzato come preview
        //((ImageClipboardItem *)_item)->setImage();
        break;
    case Type::Video:
        qxtLog->info() << __FILE__ << "--Instancing new VideoClipboardItem.\t(Video)";
        //! @todo Oggetto QVideo @sa Libreria Phonon Qt, oggetto Phonon::VideoWidget...
        _item = new VideoClipboardItem();
        _item->setName(_fileInfo.completeBaseName());
        break;
    case Type::Directory:
        qxtLog->info() << __FILE__ << "--Instancing new DirectoryClipboardItem.\t(Directory)";
        _item = new DirectoryClipboardItem();
        _item->setName(_fileInfo.dir().dirName());
        //! @todo Lista di tutti i files presenti nella directory
        //((DirectoryClipboardItem *)_item)->setEntryList();
        break;
    case Type::UnknownFile:
        qxtLog->info() << __FILE__ << "--Instancing new GenericClipboardItem.\t(UnknownFile)";
        _item = new GenericClipboardItem();
        _item->setName(_fileInfo.completeBaseName());
        break;
    default:
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Unhandled case in switch (while creating instance for FileType)";
        break;
    }

    _item->setIdentifier(ItemUtils::calculateFileCRC16(_fileInfo));
    //! @bug Non devo aggiungere mime data, non ha assolutamente senso, al massimo mi ricostruisco un QMimeData \n
    //!      senza tutti gli url che non servono a una minchia di nulla
    _item->setData(accessibleMimeData(_data));
    _item->setTimestamp(QDateTime::currentDateTime());

    return _item;
}

ClipboardItem * ItemDefinition::instanceForMultipleSelection(const QMimeData *_data)
{
    ClipboardItem * _item = NULL;

    _item = new MultiClipboardItem();

    QString _itemName;  // Stringa con il nome dell'item (verra riempito con le occorrenze dei tipi)
    QMap<QString, int> _extensionOccurrences; // Numero di volte che c'e' un'estensione

    // Controllo i tipi di file
    QFileInfo _currentFileInfo;
    foreach(QUrl _url, _data->urls())
    {
        _currentFileInfo = _url.toLocalFile();
        // Aggiorno la lista delle occorrenze dell'estensione
        if(!_currentFileInfo.isDir())
        {
            if(_extensionOccurrences.value(_currentFileInfo.suffix()) == 0)
                _extensionOccurrences.insert(_currentFileInfo.suffix(), 1);
            else
               _extensionOccurrences[_currentFileInfo.suffix()]++;
        }
        else
        {
            if(_extensionOccurrences.value("Dir") == 0)
                _extensionOccurrences.insert("Dir", 1);
            else
                _extensionOccurrences["Dir"]++;
        }
    }
    // Creo il nome dell'oggetto
    foreach(QString _key, _extensionOccurrences.keys())
    {
        _itemName += QString("%1(s): %2 - ").arg(_key).arg(_extensionOccurrences.value(_key));
    }
    _itemName.remove(_itemName.length() - 2, 2); // rimuovo i caratteri superflui... vedi la "-" e lo spazio (" ")

    _item->setName(_itemName);
    _item->setIdentifier(ItemUtils::calculateCollectionCRC16(_data));
    _item->setData(accessibleMimeData(_data));
    _item->setTimestamp(QDateTime::currentDateTime());

    return _item;
}

ClipboardItem * ItemDefinition::instanceForTextType(Type::eText _type, const QMimeData *_data)
{
    ClipboardItem * _item = NULL;

    QString _name = _data->text().simplified().trimmed();
    if(_name.length() > 99) { //! @todo Permettere di configurare i caratteri visualizzati nel menu'
        _name.resize(99-3);
        _name.append("...");
    }

    switch(_type)
    {
    case Type::PlainText:
        qxtLog->info() << __FILE__ << "---Instancing new TextClipboardItem.\t(PlainText)\t(hasHtml = false)";
        _item = new TextClipboardItem();
        _item->setName(_name);
        ((TextClipboardItem *)_item)->setHasHtml(false);
        break;
    case Type::Html:
        qxtLog->info() << __FILE__ << "---Instancing new TextClipboardItem.\t(Html)\t(hasHtml = true)";
        _item = new TextClipboardItem();
        _item->setName(_name);
        ((TextClipboardItem *)_item)->setHasHtml(true);
        break;
    default:
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Unhandled case in switch (while creating instance for TextType)";
        break;
    }

    std::string _textData = ((TextClipboardItem *)_item)->hasHtml() ?   _data->html().toStdString():
                                                                        _data->text().toStdString();
    _item->setIdentifier(qChecksum(_textData.c_str(), _textData.size()));
    //! @bug A cosa cazzo mi serve il mime data per il testo? "Misssshtero" xD
    _item->setData(accessibleMimeData(_data));
    _item->setTimestamp(QDateTime::currentDateTime());

    return _item;
}


QMimeData * ItemDefinition::accessibleMimeData(const QMimeData *_data)
{
    QMimeData  *_accessibleData = new QMimeData;
    foreach(QString _format, _data->formats())
    {
        _accessibleData->setData(_format, _data->data(_format));
    }
    // Da controllare se questi SET servono o meno
    if(_data->hasImage())
    {
        _accessibleData->setImageData(_data->imageData());
    }
    if(_data->hasColor())
    {
        _accessibleData->setColorData(_data->colorData());
    }
    if(_data->hasUrls())
    {
        _accessibleData->setUrls(_data->urls());
    }
    if(_data->hasText())
    {
        _accessibleData->setText(_data->text());
    }
    if(_data->hasHtml())
    {
        _accessibleData->setHtml(_data->html());
    }
    //
    return _accessibleData;
}
