#include "itemlookup.h"

//+SINGLE+ITEM+//
bool ItemLookup::isItemAlreadyCopied(const QMimeData * _data, QSet<quint16> * _itemsCrc)
{
    if(_itemsCrc->contains(ItemUtils::calculateCRC16(_data)))
    {
        return true;
    }
    else
    {
        return false;
    }
}
//-SINGLE-ITEM-//

//+COLLECTION+//
bool ItemLookup::isCollectionAlreadyCopied(const QMimeData *_data, QSet<quint16> *_itemsCrc)
{
    if(_itemsCrc->contains(ItemUtils::calculateCollectionCRC16(_data)))
    {
        return true;
    }
    else
    {
        return false;
    }
}

//-COLLECTION-//

//+MULTIPLE+ITEMs+(DEPRECATED)//

QVector<bool> ItemLookup::areItemsAlreadyCopied(const QMimeData *_data, QSet<quint16> *_itemsCrc)
{
    // Crea un vettore dove verranno messi flag true o false se l'item e' stato copiato o meno
    QVector<bool>  _alreadyCopiedItems;

    switch(TypeRecognition::getGenericMimeType(_data))
    {
    case Type::Text:
        // Non dovrebbe mai andare qua dentro, se ci va c'e' un errore
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Wrong assertion, multiple items shouldn't be text items";
        break;
    case Type::File:
    {
        QFileInfo _fileInfo;
        quint16 _currentCrc;
        for(quint16 i = 0; i < _data->urls().count(); i++)
        {
            _fileInfo.setFile(_data->urls().at(i).toLocalFile());
            _currentCrc = ItemUtils::calculateFileCRC16(_fileInfo);
            if(_itemsCrc->contains(_currentCrc))
            {
                _alreadyCopiedItems << true;
            }
            else
            {
                _alreadyCopiedItems << false;
            }
        }
        break;
    }
    case Type::Unknown:
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Couldn't recognize MimeType, wrong assertion brought program here (Unknown)";
        break;
    default:
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) <<"---Unhandled case in switch (while getting generic type)";
        break;
    }


    return _alreadyCopiedItems;
}

//-MULTIPLE-ITEMs-//

