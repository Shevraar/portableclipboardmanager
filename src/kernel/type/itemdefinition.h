/*! \brief  Namespace dedicato alla creazione dell'istanza di un item di ClipboardMenu
  *
  * \author Marco Accorinti
  * \date   02/05/2011
  *
  * Questo namespace individua il tipo di dati presenti nella clipboard (passata attraverso\n
  * QMimeData) e crea un istanza dell'item (derivata da QWidgetAction) che andra' a finire nel\n
  * ClipboardMenu (della libreria ClipboardUI)
  */

#ifndef ITEMDEFINITION_H
#define ITEMDEFINITION_H

// CORE //
#include <QtCore/QSet>
#include <QtCore/QMimeData>
#include <QtCore/QUrl>
#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtCore/QDateTime>
// GUI //
#include <QtGui/QImage>

// TYPE RECOGNITION //
#include "typerecognition.h"

// ITEM LOOKUP //
#include "itemlookup.h"

// ITEMS //
#include "ClipboardItems/include/all.h"

// COMMON DEFINES and INCLUDES //
#include "common.h"

//! Spazio dei nomi dedicato alla creazione del ClipboardItem in base al tipo passato.
namespace ItemDefinition
{
    //! Tipo dell'item in base a QMimeData
    /*!
      * @deprecated         NON piu' usato in quanto ogni file viene creato singolarmente.
      * @param[in]          _data       MimeData degli items copiato
      * @param[in, out]     _itemsCrc   Crc degli items gia presenti nel menu, modificato aggiungendo i nuovi items
      * @return             L'istanza specializzata in base al tipo riconosciuto
      */
    CLIPBOARDKERNELSHARED_EXPORT    QList<ClipboardItem *>  getItemsType(const QMimeData * _data, QSet<quint16> * _itemsCrc);

    //! Tipo dell'item in base a QMimeData
    /*!
      * @param[in]          _data       MimeData dell'item copiato
      * @param[in, out]     _itemsCrc   Crc degli items gia presenti nel menu, modificato aggiungendo i nuovi items
      * @return L'istanza specializzata in base al tipo riconosciuto
      */
    CLIPBOARDKERNELSHARED_EXPORT    ClipboardItem * getItemType(const QMimeData *_data, QSet<quint16> * _itemsCrc);

    //! Crea un istanza specializzata per il tipo di oggetto eFile che vogliamo (_type)
    /*!
      * @param[in]  _type       Tipo di oggetto eFile (vedi documentazione Type::eFile)
      * @param[in]  _fileInfo   Informazioni riguardo al/ai file/files/directory
      * @param[in]  _data       MimeData del file/files/directory, ci servira' per fare il paste dell'oggetto
      * @returns                L'istanza dell'oggetto specializzata, vedi i vari item sotto menu/item
      */
    ClipboardItem *  instanceForFileType (Type::eFile _type, QFileInfo _fileInfo, const QMimeData * _data);

    //! Crea un istanza specializzata per una selezione multipla di file
    /*!
      * @param[in]  _data       MimeData del file/files/directory, ci servira' per fare il paste dell'oggetto
      * @returns                L'istanza dell'oggetto specializzata, vedi MultiClipboardItem sotto ClipboardItems
      */
    ClipboardItem *  instanceForMultipleSelection (const QMimeData * _data);

    //! Crea un istanza specializzata per il tipo di oggetto eText che vogliamo (_type)
    /*!
      * @param[in]  _type       Tipo di oggetto eText (vedi documentazione Type::eText)
      * @param[in]  _data       MimeData del testo, ci servira' per fare il paste dell'oggetto
      * @returns                L'istanza dell'oggetto specializzata, vedi i vari item sotto menu/item
      */
    ClipboardItem *  instanceForTextType (Type::eText _type, const QMimeData * _data);

    //! Crea un oggetto QMimeData accessibile dalle classi [TIPO]ClipboardItem (es: TextClipboardItem)
    /*!
      * @param[in]  _data   QMimeData da cui fare la copia dei dati
      * @returns            Oggetto QMimeData sul quale possiamo lavorare
      */
    QMimeData *              accessibleMimeData  (const QMimeData * _data);
}

#endif // ITEMDEFINITION_H
