#ifndef DATABASE_H
#define DATABASE_H

// CORE //
#include <QtCore/QObject>

// SQL //
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlRecord>

// COMMON DEFINES AND INCLUDES //
#include "common.h"

class CLIPBOARDKERNELSHARED_EXPORT Database : public QObject
{
    Q_OBJECT
public:
    //! Costruttore generico
    Database(QObject * parent = 0);
    virtual ~Database();

    //! Tipo di connessione
    enum DatabaseType
    {
        File    = 0x0,     //!< Apre la connessione con il database FileTypes
        Unknown = 0xf      //!< Non apre la connessione con alcun database
    };

    //! Apre il database scelto
    /*!
      * @param[in]    _eDbType  Tipo di database da aprire
      * @returns                TRUE o FALSE se il database scelto e' stato aperto o meno
      */
    bool open(DatabaseType _eDbType);

    //! Ritorna l'istanza del database scelto

    //! Interroga il database scelto (con una stringa)
    /*!
      * @param[in]  _queryString    La stringa contenente la query da sottoporre al database
      * @param[in]  _eDbType        Tipo di database da interrogare
      * @returns                    La query eseguita, con i risultati
      */
    QSqlQuery query(QString _queryString, DatabaseType _eDbType);

    //! Singleton istance
    static Database * Istance;

private:
    //! Inizializza le variabili della classe
    void initialize();
    //! Distrugge le variabili della classe, deallocando la memoria
    void destroy();

    //! Crea i vari databases
    void createDatabases();

    QSqlDatabase m_FileTypes; //!< Database contenente i tipi di file (immagini/video/ecc.)
};

#endif // DATABASE_H
