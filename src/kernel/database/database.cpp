#include "database.h"

// INIZIALIZZAZIONE VARIABLE STATICA //
Database * Database::Istance = NULL;

// INITIALIZIATION E DESTRUCTION SECTION //
// CONSTRUCTOR //
Database::Database(QObject * parent) :
    QObject(parent)
{
    initialize();

    createDatabases();
}


// DESTRUCTOR //
Database::~Database()
{
    destroy();
}


// INITIALIZE //
void Database::initialize()
{
    // Per inizializzare puntatori e quant'altro
}


// DESTROY //
void Database::destroy()
{
    if(m_FileTypes.isOpen())
    {
        m_FileTypes.close();
    }
}

// CREATE DATABASES
void Database::createDatabases()
{
    m_FileTypes = QSqlDatabase::addDatabase("QSQLITE", "File_Types");
    m_FileTypes.setDatabaseName("data/file_types"); // Configurabile?

}

// END OF INITIALIZIATION E DESTRUCTION SECTION //
// PUBLIC SECTION
// OPEN //
bool Database::open(DatabaseType _eDbType)
{
    bool _result = false; // Risultato apertura database.
    switch(_eDbType)
    {
    case File:
        _result = m_FileTypes.open();
        if(!_result) // Se c'e' stato un errore
        {
            // Controllo se e' veramente un errore
            if(m_FileTypes.lastError().isValid())
                // Lo scrive sul log
                qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) << QString("---Failed to open database: %1").arg(m_FileTypes.lastError().text());
        }
        break;
    case Unknown:
        // Non fai nulla
        _result = false;
        break;
    default:
        _result = false;
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) << QString("---Unhandled case in switch (while opening database connection). Type passed was %1").arg(_eDbType);
        break;
    }
    return _result;
}

// QUERY (STRING) //
QSqlQuery Database::query(QString _queryString, DatabaseType _eDbType)
{
    QSqlQuery _query;

    // Query sul database _eType
    switch(_eDbType)
    {
    case File:
        _query = QSqlQuery(m_FileTypes);
        break;
    case Unknown:
        return NULL;
        break;
    default:
        qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) << QString("---Unhandled case in switch (while querying the database type: %1). Type passed was: %2").arg(_eDbType).arg(_queryString);
        return NULL;
        break;
    }

    // Eseguo la query con la stringa passata
    bool _result = _query.exec(_queryString);
    if(!_result) // Se c'e' stato un errore
    {
        // Controllo se e' veramente un errore
        if(_query.lastError().isValid())
        {
            // Lo scrivo sul log
            qxtLog->error() << __FILE__ << QString("--at:\t%1").arg(__LINE__) << QString("---Failed to execute query: %1").arg(_query.lastError().text());
        }
    }
    return _query;
}

// END OF PUBLIC SECTION //
