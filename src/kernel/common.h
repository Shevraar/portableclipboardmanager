/*! \file common.h
  *
  * \author Marco Accorinti
  * \date   03/05/2011
  *
  * \brief File di define e include comuni a tutto il progetto ClipboardKernel
  */

#ifndef COMMON_H
#define COMMON_H

// QXTCORE //
#include <QxtCore/QxtLogger>

#include "ClipboardKernel_global.h"


#endif // COMMON_H
