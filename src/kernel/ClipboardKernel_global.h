#ifndef CLIPBOARDKERNEL_GLOBAL_H
#define CLIPBOARDKERNEL_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CLIPBOARDKERNEL_LIBRARY)
#  define CLIPBOARDKERNELSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CLIPBOARDKERNELSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CLIPBOARDKERNEL_GLOBAL_H
