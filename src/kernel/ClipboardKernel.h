/*! \mainpage               Descrizione del progetto
  * \section    intro_sec   Introduzione
  *             Questo progetto e' stato creato per centralizzare tutte le operazioni\n
  *             fondamentali del progetto ClipboardManager.\n
  *             Per operazioni fondamentali intendo la creazione e l'assegnazione di un item\n
  *             a ClipboardMenu, la fornitura di una tastiera virtuale per rendere il processo\n
  *             di incollamento di un elemento di ClipboardMenu (un item) trasparente e indolore\n
  * \section    mod_design  Spiegazione del design modulare
  * Questo progetto e' stato pensato in modo da essere modulare, ovvero da dividere in modo netto i vari\n
  * moduli che comporranno il prodotto finale.
  *     \subsection mod_graph   Grafico dei moduli e delle dipendenze tra loro
  *         \dot
  *         digraph modgraph {
  *             Con [label="Controller Executable" fontname = "Courier New"];           // Controller Module    (Executable)
  *             Log [label="Log Module" fontname = "Courier New"];                      // Logging Module       (Library)
  *             Ker [label="Kernel Module" shape=box fontname = "Courier New"];         // Kernel Module        (Library)
  *             Ite [label="Items Module" fontname = "Courier New"];                    // Items Module         (Library)
  *             UI  [label="UI Module" fontname = "Courier New"];                       // UI (Menu) Module     (Library)
  *             Con -> UI;                                                              // Controller Dependancies 1 (to UI Module)
  *             Con -> Log;                                                             // Controller Dependancies 2 (to Log Module)
  *             UI -> Ite;                                                              // UI Dependancies 1 (to Items)
  *             UI -> Ker;                                                              // UI Dependancies 2 (to Kernel)
  *             UI -> Log;                                                              // UI Dependancies 3 (to Log)
  *             Ker -> Ite;                                                             // Kernel Dependancies 1 (to Items)
  *             Ker -> Log;                                                             // Kernel Dependancies 2 (to Log)
  *             Ite -> Log;                                                             // Items Dependancies 1 (to Log)
  *         }
  *         \enddot
  */

/*! \brief  Header del progetto ClipboardKernel
  *
  * \author Marco Accorinti
  * \date   15/05/2011
  *
  * Serve alla creazione dell'istanza di logging per il progetto ClipboardKernel
  */

#ifndef CLIPBOARDKERNEL_H
#define CLIPBOARDKERNEL_H

// CORE //
#include <QtCore/QObject>
#include <QtCore/QString>

// COMMON DEFINES AND INCLUDES //
#include "common.h"

// LOG //
#include "Log/include/cmlog.h"

// DATABASE //
#if defined(CLIPBOARDKERNEL_LIBRARY)
#   include "database/database.h"
#endif

namespace ClipboardKernel
{
    //! Inizializza il modulo clipboard kernel
    CLIPBOARDKERNELSHARED_EXPORT void Init();
    //! Distrugge il modulo clipboard kernel
    CLIPBOARDKERNELSHARED_EXPORT void Destroy();

    //! Istanza del modulo clipboard kernel, il suo utilizzo e' solo per loggare in questo modulo, per ora
    class CLIPBOARDKERNELSHARED_EXPORT Kernel
    {
    public:
        //! Costruttore generico
        Kernel();
        //! Distruttore generico
        ~Kernel();

        //! Inizia la connessione con i databases
        void openDatabases();

    private:
        //! Inizializza l'istanza clipboard kernel
        void initialize();
        //! Distrugge gli oggetti di Clipboard kernel
        void destroy();

        //! Crea log
        void createLog();

        //! Crea database
        void createDatabases();

        //! Inizia la connessione con il database dei tipi di file
        void openFileTypeDatabase();

        //! Classe di logging per il modulo clipboard kernel
        CMLog * m_log;
    };
}

#endif // CLIPBOARDKERNEL_H
