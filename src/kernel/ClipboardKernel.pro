#-------------------------------------------------
#
# Project created by QtCreator 2011-04-10T16:45:41
#
#-------------------------------------------------

QT += core sql

CONFIG += qxt #Libreria libQxt
QXT += core

TARGET = ClipboardKernel
TEMPLATE = lib

DEFINES += CLIPBOARDKERNEL_LIBRARY

VERSION += 0.0.7.2

SOURCES += type/typerecognition.cpp \
    type/itemdefinition.cpp \
    type/itemlookup.cpp \
    type/itemutils.cpp \
    ClipboardKernel.cpp \
    database/database.cpp

HEADERS +=   ClipboardKernel_global.h \
    type/typerecognition.h \
    type/itemdefinition.h \
    type/itemlookup.h \
    type/itemutils.h \
    ClipboardKernel.h \
    common.h \
    database/database.h

# Libraries includepath
# QtLibs e' il percorso dove si trovano tutte le librerie aggiuntive che usero'
win32 {
    win32:INCLUDEPATH += $$(HOMEDRIVE)/QtLibs/
}
unix {
    unix:INCLUDEPATH += $$(HOME)/Developing/QtLibs/
}

# LINKING & TARGETING
CONFIG(debug, debug|release) {
    #DEBUG
    TARGET = $$join(TARGET,,,d)
    win32 {
        win32-msvc2008|win32-msvc2010 {
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardItems/msvc/ -lClipboardItemsd0 #MSVC
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/msvc/ -llogd0 #MSVC
        }
        win32-g++ {
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardItems/mingw-32/ -lClipboardItemsd0 #MINGW32
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/mingw-32/ -llogd0 #MINGW32
        }
    }
    unix {
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/ClipboardItems/ -lClipboardItemsd
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/Log/ -llogd
    }
} else {
   #RELEASE
    win32 {
        win32-msvc2008|win32-msvc2010 {
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardItems/msvc/ -lClipboardItems0 #MSVC
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/msvc/ -llog0 #MSVC
        }
        win32-g++ {
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/ClipboardItems/mingw-32/ -lClipboardItems0 #MINGW32
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/mingw-32/ -llog0 #MINGW32
        }
    }
    unix {
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/ClipboardItems/ -lClipboardItems
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/Log/ -llog
    }
}


#INSTALLAZIONE TARGET (*.dll, *.lib, *.pdb, *.a, etc)
win32 {
    win32-msvc2008|win32-msvc2010 {
        win32:target.path = $$(HOMEDRIVE)/QtLibs/ClipboardKernel/msvc #MSVC
    }
    win32-g++ {
        win32:target.path = $$(HOMEDRIVE)/QtLibs/ClipboardKernel/mingw-32 #MINGW32
    }
}
unix {
    unix:target.path =  $$(HOME)/Developing/QtLibs/ClipboardKernel/
}

#INSTALLAZIONE HEADERS
header_files.files = $$HEADERS

win32 {
    win32:header_files.path = $$(HOMEDRIVE)/QtLibs/ClipboardKernel/include
}
unix {
    unix:header_files.path = $$(HOME)/Developing/QtLibs/ClipboardKernel/include
}

#INSTALLS e' chiamato quando a make viene passato l'argomento "install"
INSTALLS += target header_files
