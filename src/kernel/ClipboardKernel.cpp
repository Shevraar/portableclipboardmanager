#include "ClipboardKernel.h"

namespace ClipboardKernel
{
    //! Istanza della libreria Kernel
    Kernel * Istance;
}

// CLIPBOARD KERNEL NAMESPACE //

void ClipboardKernel::Init()
{
    ClipboardKernel::Istance = new ClipboardKernel::Kernel();
    ClipboardKernel::Istance->openDatabases();
}

void ClipboardKernel::Destroy()
{
    delete ClipboardKernel::Istance;
}

// CLIPBOARD KERNEL NAMESPACE //

// KERNEL CLASS //

ClipboardKernel::Kernel::Kernel()
{
    initialize();

    createLog();
    createDatabases();
}

ClipboardKernel::Kernel::~Kernel()
{
    destroy();
}

void ClipboardKernel::Kernel::initialize()
{
    m_log = NULL;
    Database::Istance = NULL;
}

void ClipboardKernel::Kernel::destroy()
{
    if(m_log)
    {
        delete m_log;
        m_log = NULL;
    }
    if(Database::Istance)
    {
        delete Database::Istance;
        Database::Istance = NULL;
    }
}

void ClipboardKernel::Kernel::createLog()
{
    m_log = new CMLog("Kernel", "Clipboard");
}

void ClipboardKernel::Kernel::createDatabases()
{
    Database::Istance = new Database();
}

void ClipboardKernel::Kernel::openDatabases()
{
    // Per ora apriamo solo una connessione, ma successivamente sara'
    // possibile aprire connessioni con piu' databases
    openFileTypeDatabase();
}

void ClipboardKernel::Kernel::openFileTypeDatabase()
{
    // Apre la connessione con il database dei tipi di file
    bool _result = Database::Istance->open(Database::File);
    if(!_result)
    {
        qxtLog->error() << __FILE__ << __LINE__ << "---There was an error opening connection with files types database.";
    }
}

// KERNEL CLASS //
