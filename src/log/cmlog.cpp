#include "cmlog.h"

//! Inizializza la variabile statica contenente il nome dell LoggerEngine
QString CMLog::m_CMLoggerEngineName = "";
//! Inizializza la variabile statica contenente l'oggetto LoggerEngine
QxtBasicFileLoggerEngine * CMLog::m_CMLoggerEngine = NULL;

//+CONSTRUCTOR
CMLog::CMLog(QString _moduleName, QString _loggingEngineName, QObject *parent)
    :QObject(parent)
{
    setModuleName(_moduleName);
    setLoggerEngineName(_loggingEngineName);

    initialize();
    createObjects();

    qxtLog->write() << QString("************ LOGGING FOR %1 STARTED ************")
                       .arg(moduleName().toUpper(), 20);
}
//-CONSTRUCTOR

//+DESTRUCTOR
CMLog::~CMLog()
{
    destroyObjects();
}

//-DESTRUCTOR

//+INITIALIZE
void CMLog::initialize()
{
    m_CMLoggerEngine = NULL;
}

//-INITIALIZE

//+DESTROY
void CMLog::destroyObjects()
{
    qxtLog->write() << QString("************ LOGGING FOR %1 STOPPED ************")
                       .arg(moduleName().toUpper(), 20);
    if(m_CMLoggerEngine)
    {
        delete m_CMLoggerEngine;
        m_CMLoggerEngine = NULL;
    }
    if(!loggerEngineName().isEmpty())
        setLoggerEngineName("");
}

//-DESTROY

//+CREATE
void CMLog::createObjects()
{
    createLogger();
}

//+LOGGER
void CMLog::createLogger()
{
    //! @todo Migliorare il nome del file.
    m_CMLoggerEngine = new QxtBasicFileLoggerEngine(QString("log/%1.log")
                                              .arg(loggerEngineName()));
    m_CMLoggerEngine->setDateFormat("yyyy-MM-ddThh:mm:ss.zzz");

    //Assegna l'engine di logging appena creato alla macro qxtLog.
    qxtLog->addLoggerEngine(loggerEngineName(), m_CMLoggerEngine);
    qxtLog->enableLoggerEngine(loggerEngineName());
    //! @todo Permettere di configurare il livello di logging (solo utenti avanzati)
    qxtLog->enableAllLogLevels();
}

//-LOGGER
//-CREATE
