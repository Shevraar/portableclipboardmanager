#-------------------------------------------------
#
# Project created by QtCreator 2011-04-21T21:28:39
#
#-------------------------------------------------

VERSION = 0.0.4

QT       -= gui
QT       += core

CONFIG += qxt #Libreria libQxt
QXT += core

TARGET = log
TEMPLATE = lib

DEFINES += LOG_LIBRARY

SOURCES += cmlog.cpp

HEADERS += cmlog.h\
        log_global.h

#TARGET LOCALE
CONFIG(debug, debug|release) {
   #DEBUG
   TARGET = $$join(TARGET,,,d)
} else {
   #RELEASE
}

#INSTALLAZIONE TARGET (*.dll, *.lib, *.pdb, *.a, etc)
win32 {
    win32-msvc2008|win32-msvc2010 {
        win32:target.path = $$(HOMEDRIVE)/QtLibs/Log/msvc #MSVC
    }
    win32-g++ {
        win32:target.path = $$(HOMEDRIVE)/QtLibs/Log/mingw-32 #MINGW32
    }
}
unix {
    unix:target.path =  $$(HOME)/Developing/QtLibs/Log/
}

#INSTALLAZIONE HEADERS
header_files.files = $$HEADERS

win32 {
    win32:header_files.path = $$(HOMEDRIVE)/QtLibs/Log/include
}
unix {
    unix:header_files.path = $$(HOME)/Developing/QtLibs/Log/include
}

#INSTALLS e' chiamato quando a make viene passato l'argomento "install"
INSTALLS += target header_files
