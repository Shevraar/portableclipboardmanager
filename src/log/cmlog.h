/*! \brief Classe di logging.
  *
  * \author Marco Accorinti
  * \date   13/04/2011
  *
  * Inserito engine per loggare, che usa la classe <tt><b>QxtLogger</b></tt> per scrivere un file di log per ogni evento.
  * @todo Dare un limite di grandezza al file di log. Rinominare in %NOMEFILE%.LOG.OLD il file di logging vecchio.
  */

#ifndef CMLOG_H
#define CMLOG_H

// Per esportare la libreria
#include "log_global.h"

// CORE //
#include <QtCore/QObject>
#include <QtCore/QString>

// QXT CORE  //
#include <QxtCore/QxtLogger>
#include <QxtCore/QxtBasicFileLoggerEngine>

class LOGSHARED_EXPORT CMLog : public QObject {
	Q_OBJECT
public:
    //! Costruttore con nome del log
    /*!
      * @param[in]  _moduleName             Nome del modulo nel quale e' istanziata questa classe
      * @param[in]  _loggingEngineName      Nome dell'engine di logging
      * @param[in]  parent                  Genitore dell'oggetto
      */
    CMLog(QString _moduleName, QString _loggingEngineName, QObject * parent = 0);
    //! Distruttore generico
    virtual ~CMLog();

    //! Setta il nome del logging engine
    void setLoggerEngineName(QString _loggingEngineName) { CMLog::m_CMLoggerEngineName = _loggingEngineName; }
    //! Rende il nome del logging engine, in modo easy
    QString loggerEngineName() const { return CMLog::m_CMLoggerEngineName; }

    //! Setta il nome del modulo in cui e' istanziata questa classe
    void setModuleName(QString _moduleName) { m_CMModuleName = _moduleName; }
    //! Rende il nome dle modulo in cui e' istanziata questa classe
    QString moduleName() const { return m_CMModuleName; }

private:
    //! Inizializza gli oggetti della classe
    void initialize();
    //! Crea gli oggetti della classe
    void createObjects();
    //! Distrugge gli oggetti della classe
    void destroyObjects();

    //! Crea il file di logging
    void createLogger();

    // LOGGING ENGINE //
    static QString                    m_CMLoggerEngineName;    //!< Nome per il logging engine
    static QxtBasicFileLoggerEngine * m_CMLoggerEngine;        //!< LoggerEngine

    // MODULE //
    QString                           m_CMModuleName;           //!< Nome del modulo in cui e' istanziata questa classe
};

#endif // CMLOG_H
