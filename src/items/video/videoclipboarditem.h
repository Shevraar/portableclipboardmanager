#ifndef VIDEOCLIPBOARDITEM_H
#define VIDEOCLIPBOARDITEM_H

#if defined(CLIPBOARDITEMS_LIBRARY)
#   include "../clipboarditem.h"
#else
#   include "clipboarditem.h"
#endif

class CLIPBOARDITEMSSHARED_EXPORT VideoClipboardItem : public ClipboardItem
{
public:
    //! Costruttore di default
    /*!
      * @param[in]  parent   Genitore dell'oggetto
      */
    VideoClipboardItem(QObject * parent = 0);

    //! Costruttore di copia
    /*!
      * @param[in]  other   Altro oggetto VideoClipboardItem
      */
    VideoClipboardItem(const VideoClipboardItem &other);
};

#endif // VIDEOCLIPBOARDITEM_H
