#include "videoclipboarditem.h"

VideoClipboardItem::VideoClipboardItem(QObject * parent):
    ClipboardItem(parent)
{
    setIcon(QIcon(":/video/icon"));
}

VideoClipboardItem::VideoClipboardItem(const VideoClipboardItem &other):
    ClipboardItem(other)
{
    setIcon(other.icon());
}
