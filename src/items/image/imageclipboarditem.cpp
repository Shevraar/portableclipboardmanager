#include "imageclipboarditem.h"

ImageClipboardItem::ImageClipboardItem(QObject * parent):
    ClipboardItem(parent)
{
    setIcon(QIcon(":/image/png")); //! @todo Implementare riconoscimento immmagine (esempio passare estensione immagine nel costruttore)
}


ImageClipboardItem::ImageClipboardItem(const ImageClipboardItem &other):
    ClipboardItem(other)
{
    setIcon(other.icon());
}
