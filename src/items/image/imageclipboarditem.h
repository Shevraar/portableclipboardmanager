#ifndef IMAGECLIPBOARDITEM_H
#define IMAGECLIPBOARDITEM_H

#if defined(CLIPBOARDITEMS_LIBRARY)
#   include "../clipboarditem.h"
#else
#   include "clipboarditem.h"
#endif

class CLIPBOARDITEMSSHARED_EXPORT ImageClipboardItem : public ClipboardItem
{
public:
    //! Costruttore generico
    /*!
      * @param[in]  parent  Genitore dell'oggetto.
      */
    ImageClipboardItem(QObject * parent = 0);

    //! Costruttore di copia
    /*!
      * @param[in]  other   Altro oggetto ImageClipboardItem
      */
    ImageClipboardItem(const ImageClipboardItem &other);
};

#endif // IMAGECLIPBOARDITEM_H
