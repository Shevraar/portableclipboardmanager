#include "textclipboarditem.h"

TextClipboardItem::TextClipboardItem(QObject * parent):
    ClipboardItem(parent)
{
    if(hasHtml())
    {
        setIcon(QIcon(":/text/html"));
    }
    else
    {
        setIcon(QIcon(":/text/plain"));
    }
}


TextClipboardItem::TextClipboardItem(const TextClipboardItem &other):
    ClipboardItem(other)
{
    setIcon(other.icon());
}

