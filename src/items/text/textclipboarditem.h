#ifndef TEXTCLIPBOARDITEM_H
#define TEXTCLIPBOARDITEM_H

#if defined(CLIPBOARDITEMS_LIBRARY)
#   include "../clipboarditem.h"
#else
#   include "clipboarditem.h"
#endif

class CLIPBOARDITEMSSHARED_EXPORT TextClipboardItem : public ClipboardItem
{
public:
    //! Costruttore di default
    /*!
      * @param[in]  parent   Genitore dell'oggetto
      */
    TextClipboardItem(QObject * parent = 0);

    //! Costruttore di copia
    /*!
      * @param[in]  other   Altro oggetto TextClipboardItem
      */
    TextClipboardItem(const TextClipboardItem &other);

    // SET delle variabili
    //! Setta se ha html dentro il testo
    /*!
      * @param[in] _bHasHtml    TRUE o FALSE se ha o meno html nel testo
      */
    void setHasHtml(bool _bHasHtml) { m_bHasHtml = _bHasHtml; }

    // GET delle variabili
    //! C'e' dell'html dentro il testo?
    /*!
      * @returns TRUE o FALSE se ha o meno html
      */
    bool hasHtml() const { return m_bHasHtml; }

private:

    //! L'item contiene html?
    bool    m_bHasHtml;
};

#endif // TEXTCLIPBOARDITEM_H
