#ifndef ALL_H
#define ALL_H

#if !defined(CLIPBOARDITEMS_LIBRARY)
#   include "clipboarditem.h"
#   include "directoryclipboarditem.h"
#   include "genericclipboarditem.h"
#   include "imageclipboarditem.h"
#   include "multiclipboarditem.h"
#   include "textclipboarditem.h"
#   include "videoclipboarditem.h"
#endif

#endif // ALL_H
