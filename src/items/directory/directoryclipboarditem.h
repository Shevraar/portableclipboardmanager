#ifndef DIRECTORYCLIPBOARDITEM_H
#define DIRECTORYCLIPBOARDITEM_H

#if defined(CLIPBOARDITEMS_LIBRARY)
#   include "../clipboarditem.h"
#else
#   include "clipboarditem.h"
#endif

class CLIPBOARDITEMSSHARED_EXPORT DirectoryClipboardItem : public ClipboardItem
{
public:
    //! Costruttore generico
    /*!
      * @param[in]  parent  Genitore dell'oggetto.
      */
    DirectoryClipboardItem(QObject * parent = 0);

    //! Costruttore di copia
    /*!
      * @param[in]  other   Altro oggetto DirectoryClipboardItem
      */
    DirectoryClipboardItem(const DirectoryClipboardItem &other);
};

#endif // DIRECTORYCLIPBOARDITEM_H
