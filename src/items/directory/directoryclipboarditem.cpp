#include "directoryclipboarditem.h"

DirectoryClipboardItem::DirectoryClipboardItem(QObject * parent):
    ClipboardItem(parent)
{
    setIcon(QIcon(":/directory/icon"));
}


DirectoryClipboardItem::DirectoryClipboardItem(const DirectoryClipboardItem &other):
    ClipboardItem(other)
{
    setIcon(other.icon());
}
