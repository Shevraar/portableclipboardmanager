#ifndef MULTICLIPBOARDITEM_H
#define MULTICLIPBOARDITEM_H

#if defined(CLIPBOARDITEMS_LIBRARY)
#   include "../clipboarditem.h"
#else
#   include "clipboarditem.h"
#endif

class CLIPBOARDITEMSSHARED_EXPORT MultiClipboardItem : public ClipboardItem
{
public:
    //! Costruttore di default
    /*!
      * @param[in]  parent   Genitore dell'oggetto
      */
    MultiClipboardItem(QObject * parent = 0);

    //! Costruttore di copia
    /*!
      * @param[in]  other   Altro oggetto VideoClipboardItem
      */
    MultiClipboardItem(const MultiClipboardItem &other);
};

#endif // MULTICLIPBOARDITEM_H
