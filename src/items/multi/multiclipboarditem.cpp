#include "multiclipboarditem.h"

MultiClipboardItem::MultiClipboardItem(QObject * parent) :
    ClipboardItem(parent)
{
    setIcon(QIcon(":/multi/icon"));
}

MultiClipboardItem::MultiClipboardItem(const MultiClipboardItem &other) :
    ClipboardItem(other)
{
    setIcon(other.icon());
}
