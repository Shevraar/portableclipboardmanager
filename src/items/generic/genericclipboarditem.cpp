#include "genericclipboarditem.h"

GenericClipboardItem::GenericClipboardItem(QObject * parent):
    ClipboardItem(parent)
{
    setIcon(QIcon(":/generic/unknown"));
}

GenericClipboardItem::GenericClipboardItem(const GenericClipboardItem &other):
    ClipboardItem(other)
{
    setIcon(other.icon());
}
