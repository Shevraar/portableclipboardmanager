#ifndef GENERICCLIPBOARDITEM_H
#define GENERICCLIPBOARDITEM_H

#if defined(CLIPBOARDITEMS_LIBRARY)
#   include "../clipboarditem.h"
#else
#   include "clipboarditem.h"
#endif

class CLIPBOARDITEMSSHARED_EXPORT GenericClipboardItem : public ClipboardItem
{
public:
    //! Costruttore generico
    /*!
      * @param[in]  parent  Genitore dell'oggetto.
      */
    GenericClipboardItem(QObject * parent = 0);

    //! Costruttore di copia
    /*!
      * @param[in]  other   Altro oggetto GenericClipboardItem
      */
    GenericClipboardItem(const GenericClipboardItem &other);
};

#endif // GENERICCLIPBOARDITEM_H
