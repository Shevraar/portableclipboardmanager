#include "clipboarditem.h"

//+CONSTRUCTORS

ClipboardItem::ClipboardItem(QObject * parent):
    QWidgetAction(parent)
{
    initialize();
}


ClipboardItem::ClipboardItem(const ClipboardItem &other):
    QWidgetAction(other.parent())
{
    setName(other.name());
    setData(other.data());
    setTimestamp(other.timestamp());
}
//-CONSTRUCTORS
//+DESTRUCTORS

ClipboardItem::~ClipboardItem()
{
    if(m_Data)
    {
        delete m_Data;
        m_Data = NULL;
    }
}
//-DESTRUCTORS

//+OPERATOR =
ClipboardItem& ClipboardItem::operator =(const ClipboardItem &other)
{
    setParent(other.parent());
    setName(other.name());
    setData(other.data());
    setTimestamp(other.timestamp());
    return *this;
}
//-OPERATOR =

//+INITIALIZE
void ClipboardItem::initialize()
{
    setName("");
    setData(NULL);
    setTimestamp(QDateTime());
}

//-INITIALIZE
