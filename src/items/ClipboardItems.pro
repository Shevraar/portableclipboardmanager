#-------------------#
#                   #
#  CLIPBOARD ITEMS  #
#                   #
#-------------------#

VERSION = 0.0.4.7

QT       += core gui

CONFIG += qxt #Libreria libQxt per log.
QXT += core

TARGET = ClipboardItems
TEMPLATE = lib

DEFINES += CLIPBOARDITEMS_LIBRARY

SOURCES += \
    directory/directoryclipboarditem.cpp \
    generic/genericclipboarditem.cpp \
    image/imageclipboarditem.cpp \
    text/textclipboarditem.cpp \
    video/videoclipboarditem.cpp \
    multi/multiclipboarditem.cpp \
    clipboarditem.cpp

HEADERS += ClipboardItems_global.h \
    directory/directoryclipboarditem.h \
    generic/genericclipboarditem.h \
    image/imageclipboarditem.h \
    text/textclipboarditem.h \
    video/videoclipboarditem.h \
    all.h \
    multi/multiclipboarditem.h \
    clipboarditem.h


# Libraries includepath
# QtLibs e' il percorso dove si trovano tutte le librerie aggiuntive che usero'
win32 {
    win32:INCLUDEPATH += $$(HOMEDRIVE)/QtLibs/
}
unix {
    unix:INCLUDEPATH += $$(HOME)/Developing/QtLibs/
}


# LINKING & TARGETING
CONFIG(debug, debug|release) {
    #DEBUG
    TARGET = $$join(TARGET,,,d)
    win32 {
        win32-msvc2008|win32-msvc2010 {
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/msvc/ -llogd0 #MSVC
        }
        win32-g++ {
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/mingw-32/ -llogd0 #MINGW32
        }
    }
    unix {
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/Log/ -llogd
    }
} else {
   #RELEASE
    win32 {
        win32-msvc2008|win32-msvc2010 {
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/msvc/ -llog0 #MSVC
        }
        win32-g++ {
            win32:LIBS += -L$$(HOMEDRIVE)/QtLibs/Log/mingw-32/ -llog0 #MINGW32
        }
    }
    unix {
        unix:LIBS += -L$$(HOME)/Developing/QtLibs/Log/ -llog
    }
}

#INSTALLAZIONE TARGET (*.dll, *.lib, *.pdb, *.a, etc)
win32 {
    win32-msvc2008|win32-msvc2010 {
        win32:target.path = $$(HOMEDRIVE)/QtLibs/ClipboardItems/msvc #MSVC
    }
    win32-g++ {
        win32:target.path = $$(HOMEDRIVE)/QtLibs/ClipboardItems/mingw-32 #MINGW32
    }
}
unix {
    unix:target.path =  $$(HOME)/Developing/QtLibs/ClipboardItems/
}

#INSTALLAZIONE HEADERS
header_files.files = $$HEADERS

win32 {
    win32:header_files.path = $$(HOMEDRIVE)/QtLibs/ClipboardItems/include
}
unix {
    unix:header_files.path = $$(HOME)/Developing/QtLibs/ClipboardItems/include
}

#INSTALLS e' chiamato quando a make viene passato l'argomento "install"
INSTALLS += target header_files

RESOURCES += \
    items_resources.qrc
