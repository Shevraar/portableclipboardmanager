/*! \brief Interfaccia da cui derivano gli item presenti in ClipboardMenu.
  *
  * Questa classe � un'astrazione di un item presente nella clipboard, e quindi in ClipboardMenu.
  * Genericamente ci� che � presente in questa classe astratta sar� presente (in modo
  * pi� o meno uguale) anche nelle classi derivate da essa.
  *
  */
#ifndef CLIPBOARDITEM_H
#define CLIPBOARDITEM_H

// Per esportare la libreria.
#include "ClipboardItems_global.h"

// CORE //
#include <QtCore/QMimeData>
#include <QtCore/QString>
#include <QtCore/QDateTime>

// GUI //
#include <QtGui/QWidgetAction>

class CLIPBOARDITEMSSHARED_EXPORT ClipboardItem : public QWidgetAction {
public:
    //! Costruttore generico
    /*!
      * @param[in]  parent  Genitore dell'oggetto.
      */
    ClipboardItem(QObject * parent = 0);
    //! Costruttore di copia
    /*!
      * @param[in]  other   Altro oggetto clipboarditem.h
      */
    ClipboardItem(const ClipboardItem &other);

    //! Distruttore generico
    virtual ~ClipboardItem();

    // GET delle variabili
    quint16     identifier() const  { return m_Identifier; } //!< Funzione che ritorna l'indetificatore dell'item
    QString     name() const        { return m_Name; }       //!< Funzione che ritorna il nome dell'item
    QMimeData*  data() const        { return m_Data; }       //!< Funzione che ritorna i dati presenti nell'item
    QDateTime   timestamp() const   { return m_Timestamp; }  //!< Funzione che ritorna la data di copia

    // SET delle variabili
    void        setIdentifier   (quint16        _identifier)    { m_Identifier = _identifier; }         //!< Funzione che setta l'identificatore dell'item
    void        setName         (QString        _name)          { m_Name = _name; setText(m_Name); }    //!< Funzione che setta il nome dell'item
    void        setData         (QMimeData*     _data)          { m_Data = _data; }                     //!< Funzione che setta i dati presenti nell'item
    void        setTimestamp    (QDateTime      _timestamp)     { m_Timestamp = _timestamp; }           //!< Funzione che setta la data di copia

    // OPERATORS
    //! Funzione OPERATOR =
    /*!
      * Assegna una copia di un oggetto clipboarditem.h gia esistente.
      * @param[in]  &other  Reference dell'oggetto
      * @returns            Copia dell'oggetto.
      */
    ClipboardItem& operator =(const ClipboardItem &other);

protected:
    //! Inizializza le variabili/oggetti della classe
    void initialize();

    quint16             m_Identifier;   //!< Identificatore dell'item (CRC-16 checksum) @sa qChecksum(...) per la documentazione
    QString             m_Name;         //!< Nome dell'item
    QMimeData*          m_Data;         //!< Dati dell'item (Immagine, video, testo, ecc.)
    QDateTime           m_Timestamp;    //!< Data della copia

};

#endif // CLIPBOARDITEM_H
