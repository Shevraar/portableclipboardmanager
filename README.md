**Clipboard Manager Project**
=============================

_Introduzione_
--------------

Questo progetto mira a fornire all'utente un metodo per organizzare
gli item copiati nella clipboard su un menu.

_Installazione Su Linux_
------------------------

### Come fare per compilare il progetto su linux?

Per prima cosa dobbiamo essere al corrente delle dipendenze tra una
libreria e l'altra...

### Dipendenze esterne al progetto

LibQxt - `http://dev.libqxt.org/libqxt/wiki/Home`

Per compilare le librerie Qxt e' necessario avere il pacchetto di sviluppo
del sistema di finestre X11, questo pacchetto e' ottenibile attraverso il
seguente comando, nel terminale:
`sudo apt-get install libx11-dev libxtst-dev`
Dopo aver fatto cio' assicurarsi che le Qt siano nel path:
`echo $PATH`
Se il comando echo ha dato esito negativo dobbiamo aggiungere il percorso
alla variabile PATH, per farlo in modo permanente dobbiamo modificare il file
`~/.bashrc` per rispecchiare il nostro cambiamento:
Aggiungere la seguente linea al file:
`PATH="${PATH}":/home/YOUR_USERNAME/QtSDK/Desktop/Qt/CURRENT_QT_VERSION/gcc/bin`
`export PATH`
per evitare di riavviare il sistema per far caricare il nuovo PATH eseguire il comando `source ~/.bashrc`
Per assicurarci che abbiamo aggiunto le Qt al path eseguire di nuovo il comando `echo $PATH`.

Per l'installazione di LibQxt seguire le istruzioni all'url http://dev.libqxt.org/libqxt/wiki/user_guide

#### Dipendenze con le librerie del progetto

### Creazione delle cartelle necessarie (dipendenze del progetto)
Attualmente per poter buildare il progetto si utilizzano dei path assoluti e devono essere quelli per forza.

in `/home/YOUR_USERNAME/` crea una cartella **Developing** che conterra tutti i dati del progetto e le sue dipendenze. Tutto il lavoro verrà svolto in quel path.

in `/home/YOUR_USERNAME/Developing` crea la cartella **QtLibs**
in `/home/YOUR_USERNAME/Developing` crea la cartella **ClipboardManager**
clonna il progetto dentro ClipboardManager. Quindi a questo punto avrai un albero di questo genere:
 -/home/YOUR_USERNAME/Developing/ClipboardManager
						/src
						/doc
						/README.md

In `/home/YOUR_USERNAME/Developing/ClipboardManager` crea un'ulteriore cartella **bin** e in essa crea uan cartella per ogni progetto che andremmo a compilare:
**Log** 	dove avverrà l'output del progetto _Log_
**Items** 	dove avverrà l'output del progetto _Clipboarditems_
**Kernel** 	dove avverrà l'output del progetto _CLipboardKernel_
**UI**	 	dove avverrà l'output del progetto _ClipboardUI_
**Controller** 	dove avverrà l'output del progetto _ClipboardManager_

Una volta create le directory elencate sopra possiamo aprire singolarmente ogni progetto e iniziare la compilazione seguento l'ordine delle cartelle sopra.

### Dipendenze dei progetti in QtCreator

Clipboarditems 		- Log
CLipboardKernel 	- Log, Clipboarditems
ClipboardUI		- Log, Clipboarditems, CLipboardKernel
ClipboardManager	- Log, ClipboardUI


### Project Settings (Build Settings QtCreator)

Farò l'esempio per un solo progetto in quanto tutti gli altri dovranno seguire la stessa procedura. Tutte le impostazioni devono essere impostate sia per Debug che Release del progetto.

es. _Clipboarditems_
Andare su Projects, selezionare il progetto nei tab in alto, andare su Targhet > Build Settings > Build Directory e impostare il seguente percorso /home/YOUR_USERNAME/Developing/ClipboardManager/bin/Items.

Che abbiamo fatto? Abbiamo impostato come cartella di output la cartella creata in precedenza sotto bin.

Nella stessa finestra: Build Steps > Add Build Step > Make
overrida make: make
override argument: install

**Queste azioni vanno fatte per ogni singolo progetto come detto precedentemente**


### Compilazione del progetto ClipboardManager (Controller)

Per eseguire la compilazione dell'esegubile controller non basta schiacciare il bottone build e gg,
ma bisogna settare il `System Environment` in QtCreator in modo esatto.
Cosa intendo per settare l'environment in modo esatto?
Semplicemente dobbiamo aggiungere una variabile all'environment chiamata **LD_LIBRARY_PATH**, e aggiungere ad essa tutti i percorsi relativi alle librerie compilate (i file .so per intenderci).
**Esempio:**

`/home/vassi/QtSDK/Desktop/Qt/4.8.1/gcc/lib:/home/vassi/Developing/QtLibs/ClipboardUI:/home/vassi/Developing/QtLibs/ClipboardItems:/home/vassi/Developing/QtLibs/ClipboardKernel:/home/vassi/Developing/QtLibs/Log`

`/home/acco/Developing/QtLibs/ClipboardItems:/home/acco/Developing/QtLibs/ClipboardKernel:/home/acco/Developing/QtLibs/Log:/home/acco/Developing/QtLibs/ClipboardUI`
**N.B.:** __I due punti (:) sono usati come separatore tra un percorso e l'altro__.

_Installazione su Windows_
--------------------------

W.I.P. (Vedi documentazione per ora)

